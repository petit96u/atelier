CREATE TABLE Adherent(
        id_Adherent   int (11) Auto_increment  NOT NULL ,
        Num_adherent  Varchar (25) ,
        Nom           Varchar (25) ,
        Prenom        Varchar (25) ,
        Adresse       Varchar (25) ,
        Code_postal   Varchar (25) ,
        Ville         Varchar (25) ,
        Telephone     Varchar (25) ,
        Mail          Varchar (25) ,
        Mot_de_Passe  Varchar (25) ,
        date_adhesion Date ,
        PRIMARY KEY (id_Adherent )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Reservation 
#------------------------------------------------------------

CREATE TABLE Reservation(
        id_reservation int (11) Auto_increment  NOT NULL ,
        date_debut     Date ,
        date_limite    Date ,
        id_Adherent    Int ,
        id_Document    Int ,
        PRIMARY KEY (id_reservation )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Document
#------------------------------------------------------------

CREATE TABLE Document(
        id_Document  int (11) Auto_increment  NOT NULL ,
        ref_Document Varchar (25) ,
        Titre        Varchar (25) ,
        Description  Varchar (50) ,
        Auteur       Varchar (25) ,
        date_sortie  Date ,
        Etat         Int ,
        Raison       Varchar (25) ,
        id_Type      Int ,
        id_Genre     Int ,
        PRIMARY KEY (id_Document )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Genre
#------------------------------------------------------------

CREATE TABLE Genre(
        id_Genre    int (11) Auto_increment  NOT NULL ,
        label_Genre Varchar (25) ,
        PRIMARY KEY (id_Genre )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Emprunt
#------------------------------------------------------------

CREATE TABLE Emprunt(
        id_emprunt   int (11) Auto_increment  NOT NULL ,
        date_emprunt Date ,
        date_retour  Date ,
        id_Adherent  Int ,
        PRIMARY KEY (id_emprunt )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Type
#------------------------------------------------------------

CREATE TABLE Type(
        id_Type    int (11) Auto_increment  NOT NULL ,
        label_type Varchar (25) ,
        PRIMARY KEY (id_Type )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Enregistrer
#------------------------------------------------------------

CREATE TABLE Enregistrer(
        id_Document Int NOT NULL ,
        id_emprunt  Int NOT NULL ,
        PRIMARY KEY (id_Document ,id_emprunt )
)ENGINE=InnoDB;

ALTER TABLE Reservation ADD CONSTRAINT FK_Reservation_id_Adherent FOREIGN KEY (id_Adherent) REFERENCES Adherent(id_Adherent);
ALTER TABLE Reservation ADD CONSTRAINT FK_Reservation_id_Document FOREIGN KEY (id_Document) REFERENCES Document(id_Document);
ALTER TABLE Document ADD CONSTRAINT FK_Document_id_Type FOREIGN KEY (id_Type) REFERENCES Type(id_Type);
ALTER TABLE Document ADD CONSTRAINT FK_Document_id_Genre FOREIGN KEY (id_Genre) REFERENCES Genre(id_Genre);
ALTER TABLE Emprunt ADD CONSTRAINT FK_Emprunt_id_Adherent FOREIGN KEY (id_Adherent) REFERENCES Adherent(id_Adherent);
ALTER TABLE Enregistrer ADD CONSTRAINT FK_Enregistrer_id_Document FOREIGN KEY (id_Document) REFERENCES Document(id_Document);
ALTER TABLE Enregistrer ADD CONSTRAINT FK_Enregistrer_id_emprunt FOREIGN KEY (id_emprunt) REFERENCES Emprunt(id_emprunt);
