<?php

/**
 * File:  autoload.php
 * Creation Date: 21/09/2015
 * description: Fonction pour le chargement automatique des classes
 *
 * @author: boumaza
 */

function autoload( $classname ){
    $filename= '';
    $classname = ltrim( $classname , '\\');

    $filename= str_replace ('\\', DIRECTORY_SEPARATOR, $classname);
    $filename .= '.php';

    require_once $filename;
}

spl_autoload_register( 'autoload' );

