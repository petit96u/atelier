-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 07, 2015 at 05:19 PM
-- Server version: 5.1.73
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `haas36u`
--

-- --------------------------------------------------------

--
-- Table structure for table `Adherent`
--

CREATE TABLE IF NOT EXISTS `Adherent` (
  `id_Adherent` int(11) NOT NULL AUTO_INCREMENT,
  `Num_adherent` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nom` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Prenom` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Code_postal` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Ville` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Telephone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mail` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mot_de_Passe` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_adhesion` date DEFAULT NULL,
  PRIMARY KEY (`id_Adherent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `Adherent`
--

INSERT INTO `Adherent` (`id_Adherent`, `Num_adherent`, `Nom`, `Prenom`, `Adresse`, `Code_postal`, `Ville`, `Telephone`, `Mail`, `Mot_de_Passe`, `date_adhesion`) VALUES
(1, 'DGH3Z', 'Schnebelen', 'Lucie', '10, rue Calinet', '68 470', 'URBES', '01.84.02.15.62', 'schnebelen@gmail.com', '123456', '2014-10-23'),
(2, '46F2A', 'Weber', 'Valentin', '23, rue de la Cote', '70 300', 'LUXEUIL', '01.52.14.20.15', 'weber@yahoo.com', 'azerty', '2015-09-22'),
(3, 'Q523A', 'Lessard', 'Didiane', '88, Rue Joseph Vernet', '55000', 'BAR-LE-DUC', '01.10.96.47.07', 'compte@mail.fr', 'Q523A', '2015-11-07'),
(4, 'D352E', 'Blondlot', 'Dalmace', '7, Place de la Gare', '16 100', 'COGNAC', '01.10.96.47.07', 'compte@mail.fr', 'D352E', '2015-11-07'),
(5, '5G2P4', 'Duplanty', 'Charles', '9, rue des Soeurs', '83130', 'LA GARDE', '01.10.96.47.07', 'compte@mail.fr', '5G2P4', '2015-11-07'),
(6, 'L6Y25', 'Champagne', 'Bertrand', '67, rue de Lille', '62 000', 'ARRAS', '01.10.96.47.07', 'compte@mail.fr', 'L6Y25', '2015-11-07'),
(7, 'Q3K73', 'Grandpré', 'Dreux', '10, rue de la Hulotais', '69 800', 'SAINT-PRIEST', '01.10.96.47.07', 'compte@mail.fr', 'Q3K73', '2015-11-07'),
(8, 'F3E98', 'Parent', 'Carole', '20, rue Michel Ange', '76 610', 'LE HAVRE', '01.10.96.47.07', 'compte@mail.fr', 'F3E98', '2015-11-07'),
(9, '96R6L', 'Primeau', 'Aubine', '54, Chemin Des Bateliers', '74 000', 'ANNECY', '01.10.96.47.07', 'compte@mail.fr', '96R6L', '2015-11-07'),
(10, '9P4G5', 'Boileau', 'Armina', '77, rue Clement Marot', '24 000', 'PÉRIGUEUX', '01.10.96.47.07', 'compte@mail.fr', '9P4G5', '2015-11-07'),
(11, '34S8K', 'Latourelle', 'Jean', '70, rue de Lille', '62 000', 'ARRAS', '01.10.96.47.07', 'compte@mail.fr', '34S8K', '2015-11-07'),
(12, 'N764Q', 'Dubeau', 'Marc', ' 11, place de Miremont', '93 420', 'VILLEPINTE', '01.10.96.47.07', 'compte@mail.fr', 'N764Q', '2015-11-07');

-- --------------------------------------------------------

--
-- Table structure for table `Document`
--

CREATE TABLE IF NOT EXISTS `Document` (
  `id_Document` int(11) NOT NULL AUTO_INCREMENT,
  `ref_Document` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Titre` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Auteur` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_sortie` date DEFAULT NULL,
  `Etat` int(11) DEFAULT NULL,
  `Raison` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_Type` int(11) DEFAULT NULL,
  `id_Genre` int(11) DEFAULT NULL,
  `Image` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_Document`),
  KEY `FK_Document_id_Type` (`id_Type`),
  KEY `FK_Document_id_Genre` (`id_Genre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `Document`
--

INSERT INTO `Document` (`id_Document`, `ref_Document`, `Titre`, `Description`, `Auteur`, `date_sortie`, `Etat`, `Raison`, `id_Type`, `id_Genre`, `Image`) VALUES
(1, '54217', 'Le château de hurle', 'Sophie vit dans le royaume d''Ingary, un univers où la magie fait partie du quotidien.', 'Diana Wynne Jones', '1986-04-01', 1, NULL, 1, 1, 'chateau-de-hurle.jpg'),
(2, '45876', 'PHP et MySql pour les nul', 'Avec cette 5e édition de PHP et MySQL pour les Nuls, vous verrez qu''il n''est plus nécessaire d''être un as de la programmation pour développer des sites Web dynamiques et interactifs. ', 'Janet Valade', '2008-02-20', 2, NULL, 1, 5, 'php-pour-les-nuls.jpg'),
(3, '24955', 'Indiana Jones, Tome 1', 'Tout juste rentré d''une expédition, Indiana Jones est convoqué par deux militaires américains : les nazis s''intéressent à l''Arche de l''Alliance, l''objet légendaire recherché par les archéologues du monde entier ! Le coffre se trouverait en Egypte... Indy ne perd pas une minute ! Direction : Le Caire', 'Jérôme Jacobs', '2008-06-25', 1, NULL, 1, 1, 'indiana-jones-tome1.jpg'),
(4, '15986', 'Le Monde perdu', 'Le monde perdu : univers oublié, disparu ? Pas pour tous : Edouard Malone, reporter à la Daily Gazette, va tenter de prouver à la belle Gladys et à l’effrayant professeur Challenger qu’il existe encore.\nL’aventure commence dans les méandres de l’Amazone.', 'Arthur Conan Doyle', '1912-01-01', 3, 'En réparation', 1, 2, 'le-monde-perdu.jpg'),
(5, '65324', 'Chaque jour, chaque heure', 'Cela fait seize ans que Dora et Luka ne se sont pas vus, bien qu''ils aient été autrefois inséparables : enfants, ils passent leurs journées d''été ensemble sur la plage du petit village de pêcheurs croate dans lequel ils grandissent -jusqu''à ce que Dora déménage en France avec ses parents.', 'Natasa Dragnic', '2013-03-11', 1, NULL, 1, 3, 'chaque-jour-chaque-heure.jpg'),
(6, '46521', 'Les misérables', 'Je m''appelle Jean Valjean. Je suis un galérien. J''ai passé dix-neuf ans au bagne. Je suis libéré depuis quatre jours et en route pour Pontarlier qui est ma destination. Quatre jours que je marche depuis Toulon. \nAujourd''hui j''ai fait douze lieues à pied.', 'Victor Hugo', '1862-01-01', 1, NULL, 1, 4, 'les-miserables.jpg'),
(7, '65412', 'Pirates des Caraibes', 'Dans la mer des Caraïbes, au XVIIe siècle, Jack Sparrow, flibustier , voit sa vie idylle basculer le jour où son ennemi, le perfide capitaine Barbossa, lui vole son bateau, le , puis attaque la ville de Port Royal, enlevant au passage la très belle fille du gouverneur, Elizabeth Swann.', 'Gore Verbinski', '2003-01-01', 2, '', 3, 1, 'pirates-des-caraibes.jpg'),
(8, '156842', 'Star Wars, épisode VI', 'Synopsis et détails\nL''Empire galactique est plus puissant que jamais : la construction de la nouvelle arme, l''Etoile de la Mort, menace l''univers tout entier... Arrêté après la trahison de Lando Calrissian, Han Solo est remis à l''ignoble contrebandier Jabba Le Hutt.', 'J. J. Abrams', '1983-10-19', 1, NULL, 3, 2, 'star-wars-episodeVI.jpg'),
(9, '42135', 'Les quatres saisons', 'Les Quatre Saisons (dont le titre original italien est « Le quattro stagioni ») est le nom donné aux quatre concertos pour violon, composés par Antonio Vivaldi, Opus 8, no 1-4, qui ouvrent le recueil Il cimento dell''armonia e dell''invenzione — « La confrontation entre l''harmonie et l''invention ».', 'Antonio Vivaldi', '1725-01-01', 1, NULL, 2, 4, 'les-quatres-saisons.jpg'),
(10, '35678', 'Harry Potter 1', 'Après la mort tragique de Lily et James Potter, leur fils Harry est recueilli par sa tante Pétunia, la sœur de Lily et son oncle Vernon. Son oncle et sa tante, possédant une haine féroce envers les parents d''Harry, le maltraitent et laissent leur fils Dudley l''humilier.', 'J. K. Rowling', '1998-11-16', 3, 'Réservé', 1, 2, 'harry-potter1.jpg'),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `Emprunt`
--

CREATE TABLE IF NOT EXISTS `Emprunt` (
  `id_emprunt` int(11) NOT NULL AUTO_INCREMENT,
  `date_emprunt` date DEFAULT NULL,
  `date_retour` date DEFAULT NULL,
  `id_Adherent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_emprunt`),
  KEY `FK_Emprunt_id_Adherent` (`id_Adherent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Emprunt`
--

INSERT INTO `Emprunt` (`id_emprunt`, `date_emprunt`, `date_retour`, `id_Adherent`) VALUES
(1, '2015-11-07', '2015-11-22', 2),
(2, '2015-11-07', '2015-11-22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Enregistrer`
--

CREATE TABLE IF NOT EXISTS `Enregistrer` (
  `id_Document` int(11) NOT NULL,
  `id_emprunt` int(11) NOT NULL,
  PRIMARY KEY (`id_Document`,`id_emprunt`),
  KEY `FK_Enregistrer_id_emprunt` (`id_emprunt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Enregistrer`
--

INSERT INTO `Enregistrer` (`id_Document`, `id_emprunt`) VALUES
(2, 1),
(7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Genre`
--

CREATE TABLE IF NOT EXISTS `Genre` (
  `id_Genre` int(11) NOT NULL AUTO_INCREMENT,
  `label_Genre` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_Genre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `Genre`
--

INSERT INTO `Genre` (`id_Genre`, `label_Genre`) VALUES
(1, 'Action'),
(2, 'Aventure'),
(3, 'Romance'),
(4, 'Classique'),
(5, 'Informatique');

-- --------------------------------------------------------

--
-- Table structure for table `Reservation`
--

CREATE TABLE IF NOT EXISTS `Reservation` (
  `id_reservation` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` date DEFAULT NULL,
  `date_limite` date DEFAULT NULL,
  `id_Adherent` int(11) DEFAULT NULL,
  `id_Document` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_reservation`),
  KEY `FK_Reservation_id_Adherent` (`id_Adherent`),
  KEY `FK_Reservation_id_Document` (`id_Document`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Type`
--

CREATE TABLE IF NOT EXISTS `Type` (
  `id_Type` int(11) NOT NULL AUTO_INCREMENT,
  `label_type` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_Type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Type`
--

INSERT INTO `Type` (`id_Type`, `label_type`) VALUES
(1, 'Livre'),
(2, 'CD'),
(3, 'DVD');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Document`
--
ALTER TABLE `Document`
  ADD CONSTRAINT `FK_Document_id_Genre` FOREIGN KEY (`id_Genre`) REFERENCES `Genre` (`id_Genre`),
  ADD CONSTRAINT `FK_Document_id_Type` FOREIGN KEY (`id_Type`) REFERENCES `Type` (`id_Type`);

--
-- Constraints for table `Emprunt`
--
ALTER TABLE `Emprunt`
  ADD CONSTRAINT `FK_Emprunt_id_Adherent` FOREIGN KEY (`id_Adherent`) REFERENCES `Adherent` (`id_Adherent`);

--
-- Constraints for table `Enregistrer`
--
ALTER TABLE `Enregistrer`
  ADD CONSTRAINT `FK_Enregistrer_id_emprunt` FOREIGN KEY (`id_emprunt`) REFERENCES `Emprunt` (`id_emprunt`),
  ADD CONSTRAINT `FK_Enregistrer_id_Document` FOREIGN KEY (`id_Document`) REFERENCES `Document` (`id_Document`);

--
-- Constraints for table `Reservation`
--
ALTER TABLE `Reservation`
  ADD CONSTRAINT `FK_Reservation_id_Document` FOREIGN KEY (`id_Document`) REFERENCES `Document` (`id_Document`),
  ADD CONSTRAINT `FK_Reservation_id_Adherent` FOREIGN KEY (`id_Adherent`) REFERENCES `Adherent` (`id_Adherent`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
