<?php

require_once 'conf/autoload.php';

$http = new \utils\HttpRequest();

$controleur = new \medianetapp\staffapp\control\StaffController($http);
echo $controleur->dispatch();
