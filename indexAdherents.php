<?php

require_once 'conf/autoload.php';

$http = new \utils\HttpRequest();

$controleur = new \medianetapp\adherentsapp\controller\AdherentsController($http);
echo $controleur->dispatch();
