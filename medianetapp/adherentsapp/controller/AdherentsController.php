<?php
namespace medianetapp\adherentsapp\controller;


use medianetapp\adherentsapp\model\Adherent as Adherent;
use medianetapp\adherentsapp\model\Document as Documents;
use medianetapp\adherentsapp\model\Emprunt as Emprunt;
use medianetapp\adherentsapp\view\ViewAdherents as View;

class AdherentsController{
	private $action=null, $param=null;
		
	public function __construct(\utils\HttpRequest $ht_request)
		{
		$this->action= $ht_request->action;
		$this->param= $ht_request->param;
	}
		
	public function dispatch(){
		switch($this->action){
			case 'listDocuments':
			if(isset($_POST['type'])) {
				$this->searchAvancee();
			}elseif(isset($_POST['genres'])) {
				$this->searchAvancee();
			}elseif(isset($_POST['recherche'])) {
				$this->searchAvancee();
			}
			elseif(isset($_POST['mot-cle']) && $_POST['mot-cle']!=""){
				$this->search();
			}else
				
			$this->listDocuments();
			break;
			case 'reservation':
			$this->reservation();
			break;
			case 'identification':
			$this->identification();
			break;
			case 'visualiserEmprunt':
			$this->visualiserEmprunt();
			break;
			case 'findById':
			$this->findById();
            break;
			default : $this->Accueil();
			break;
		}
	}
	
	private function Accueil() {
		$list = Documents::findLastThree();
		$view = new View();
		$view->list = $list;
		$view->afficher("accueil");
	}	
	
	private function listDocuments(){
		$list = Documents::findAll();
		
		if($list) {
			$view = new View();
			$view->list=$list;
			$view->afficher("listDocuments");
		}
	}
	
	
	private function search(){
		if(isset($_POST["mot-cle"]) && $_POST["mot-cle"]!=""){
		
			$document= Documents::findByTitle($_POST["mot-cle"]);
			
			if($document){
				$view = new View();
				$view->list=$document;
				$view->afficher("listDocuments");
			}else
			echo "Aucun résultat";
		}
	}
	
		private function searchAvancee(){
		
			// si les trois champs sont remplis, on les récupère. Cela permettra d'affiner la recherche au maximum.
			/*if(isset($_POST["type"])&&(isset($_POST["genres"]))&&(isset($_POST["recherche"]))){
				$type = $_POST["type"];
				$genre = $_POST["genres"];
				$motcle = $_POST["recherche"];
				$list= Documents::findbyAll($type, $genre,$motcle);
					
				if($list){
					$view = new View();
					$view->list=$list;
					$view->afficher("listDocuments");
				}
				else{
					$view = new View();
					$view->alert("Aucun résultat all");
				}
			}*/
			if (isset($_POST["type"])) {
			$list= Documents::findbyType($_POST["type"]);
					if($list){
						$view = new View();
						$view->list=$list;
						$view->afficher("listDocuments");
					}
			}
			elseif(isset($_POST["genres"])){
				$list = Documents::findbyGenre($_POST["genres"]);
				if($list){
					$view = new View();
					$view->list=$list;
					$view->afficher("listDocuments");
				}	
				}
				if(isset($_POST["recherche"]) && $_POST["recherche"]!=""){
						
					$list = Documents::findbyTitle($_POST["recherche"]);
					if($list){
						$view = new View();
						$view->list=$list;
						$view->afficher("listDocuments");
					}
					else{
					
						$view = new View();
						$view->afficher("listDocuments");
						$view->alert("Aucun résultat recherche");
					}
				}
						
					
		}
	
	private function reservation() {
		$view = new View();
		$view->afficher("reservation");
	}
	
	private function findById() {
		$detail= Documents::findById($_GET['id']);
		if($detail){
		$view = new View();
		$view->list=$detail;
		$view->afficher("detail");}
	}
	
	private function visualiserEmprunt() {
		$view = new View();
		$view->afficher("visualiserEmprunt");
	}
	
	private function identification() {
		$view = new View();
		$view->afficher("identification");
	}
	

}