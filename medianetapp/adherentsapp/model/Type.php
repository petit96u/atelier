<?php

namespace namespace medianetapp\model;

Class Type
{
// Cette classe permet de visualiser et créer un nouveau type de média.
// Comme la classe Genre, on ne peut ni modifier ni supprimer un type de media.

// Attributs de la classe Type

private $id_Type, $libelle_Type;

public function __construct()
{
}

public function insert();
{
	$pdo= \medianetapp\connexion::getConnexion();
	
	$req= $pdo->prepare (insert into type (libelle_Type) values (:libelle_Type);
	$req->bindParam(":libelle_Type" , $libelle_Type);
	$req->execute();
	
}

public function findAll()
{
	$pdo= \medianetapp\connexion::getConnexion();
	
	$tab= array();
	$req= "Select * from type";
	$req->execute();
	$rows= $pdo->fetchAll(\PDO::FETCH_OBJ);
	foreach($rows as $t)
	{
	$o = new type();
	$o->$t['label_Type'];
	$tab[] = $o; 
	}
	return $tab[];
}

}

?>