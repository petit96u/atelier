<?php

namespace medianetapp\adherentsapp\view;

class ViewAdherents {
	private $document, $list=array();

public function __construct() {
  }

/*get et set pour accéder aux attr privés ds StaffControlleur*/
	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    throw new \Exception($emess);
	}

	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      throw new \Exception($emess);
	    }
	}
	public function afficher($nomAction) {
		
		$header=$this->afficherHeader();
		$footer=$this->afficherFooter();
		
		switch ($nomAction){
			case 'listDocuments' :
			$main=$this->listDocuments();
			break;
			case 'reservation':
			$main=$this->afficherReservation();
			break;
			case 'ficheDetaillee':
			$main=$this->ficheDetaillee();
			break;
			case 'visualiserEmprunt':
			$main=$this->visualiserEmprunt();
			break;
			case 'accueil' :
			$main=$this->afficherAccueil();
			break;
			case 'detail';
			$main=$this->afficheDetail();
			break;
			case 'identification':
			$main=$this->afficherIdentification();
			break;
		}	

		$http = new \utils\HttpRequest();

		$html="
		<!DOCTYPE html>
		<html lang='fr' xmlns='http://www.w3.org/1999/xhtml'>
		<head>
			<meta charset='utf-8'>
			<meta name='viewport' content='width=device-width ,initial-scale=1.0'>
			<link rel='stylesheet' href='/".$http->racine."/web/sass/stylesheets/alebrije_librairie.css' />
			<link rel='icon' href='/".$http->racine."/web/images/favicon.png' type='image/png' />
			<script src='/".$http->racine."/web/js/jquery.js' type='text/javascript'></script>
			<script src='/".$http->racine."/web/js/menu.js' type='text/javascript'></script>
			<title>MediaNet</title>
		</head>
		<body>
			<header id='header-adherents'>".$header."</header>"
			.$main.
			"<footer>".$footer."</footer>
		</body>
		</html>";
		echo $html;
	}

	public function afficherHeader() {
		$http = new \utils\HttpRequest();
		$html = "
		<div class='span2-ordi-5-18'>
			<a href='/".$http->racine."/indexAdherents.php' class='span2-ordi-4-23'><img src='/".$http->racine."/web/images/logoMediaNet.png' alt='logo'/></a>
			<h1 class='span2-ordi-4-23'>Plateforme pour les gestionnaires de la médiathèque</h1>
		</div>
		<div class='span3-ordi-5-18'>
			<form class='span1-1-98' id='recherche' method='POST' action=''>	<!--barre de recherche de l'entete-->
					<input name='mot-cle' type='text' placeholder='Mots-Clefs...' required /><!--
					--><input class='loupe' type='submit' value='' />
			</form>
			<div class='clear'></div>
			<nav>
				<ul>
				   <li class='active span1-ordi-4-23'><a href='/".$http->racine."/indexAdherents.php/AdherentsController/listDocuments'><span>Catalogue</span></a></li>
				   <li class='span1-ordi-4-23'><a href='/".$http->racine."/indexAdherents.php/AdherentsController/visualiserEmprunt''><span>Visualiser emprunts</span></a></li>
				   <li class='span1-ordi-4-23'><a href='/".$http->racine."/indexAdherents.php/AdherentsController/reservation''><span>Réservation</span></a></li>				  
				   <li class='span1-ordi-4-23'><a href='/".$http->racine."/indexAdherents.php/AdherentsController/identification''><span>Identification</span></a></li>
				   <div class='clear'></div>
				</ul>
			</nav>
		</div>
		<div class='clear'></div>";
		return $html;
	}

	public function afficherFooter() {
		return "<p>MediaNet.fr (c) 2015</p>";
	}

	public function listDocuments() {
		$http = new \utils\HttpRequest();
		$html = "<div class='page-liste-documents'>
			<form method='POST' action='/".$http->racine."/index.php/StaffController/listDocuments' class='span1-1-98 span1-ordi-4-23'>
			<input class='span1-1-98' name='recherche' type='text' placeholder='Mots-Clefs...' />
			
			<label class='span1-1-98'>Type : </label>
			<div class='span1-1-98'><input type='radio' name='type' value='1'>Livre</div>
			<div class='span1-1-98'><input type='radio' name='type' value='2'>CD</div><!--span1-ordi-1-98-->
			<div class='span1-1-98'><input type='radio' name='type' value='3'>DVD</div>		
			<div class='clear'></div>
			<label class='span1-1-98' for='genres'>Genre : </label>
			<select class='span1-1-98' id='genres' name='genres'>
				<option>Séléctionner un genre</option>
				<option value='1'>Action</option>
				<option value='2'>Aventure</option>
				<option value='3'>Romance</option>
				<option value='4'>Classique</option>
				<option value='5'>Informatique</option>
			</select>	
			
			<input class='span1-1-98 bouton-rechercher' id='submit' type='submit' value='Rechercher'>			
		</form>		

		<div class='span3-ordi-4-23'>";
		
		foreach ($this->list as $l) {
			$html.= "
			<section>
				<a href='findById?id=".$l->id."'><img class='span1-1-98 span1-tablette-3-31_3' src='/".$http->racine."/web/images/document/".$l->image."' alt='livre'></a>
				<div class='span2-tablette-3-31_3'>
					<div class='span2-tablette-3-31_3'>
						<h2>".$l->titre."</h2>";
			if($l->etat==1){
			$html.= "Disponible";}
			elseif($l->etat==2){
			$html.= "Emprunté jusqu'au ";
			
			}
			elseif($l->etat==3){
			$html.= "Indisponible";}
			$html.= "</p>";
			if($l->id_type==1){
					$html.= "<p>Type : Livre </p>";}
				elseif($l->id_type==2){
					$html.= "<p>Type : CD </p>";
				}
				elseif($l->id_type==3){
					$html.= "<p>Type : DVD </p>";}
				
				if($l->id_genre==1){
					$html.= "<p>Genre : Action </p>";}
				elseif($l->id_genre==2){
					$html.= "<p>Genre : Aventure </p>";
				}
				elseif($l->id_genre==3){
					$html.= "<p>Genre : Romance </p>";}
				elseif($l->id_genre==4){
					$html.= "<p>Genre : Classique </p>";}
				elseif($l->id_genre==5){
					$html.= "<p>Genre : Informatique </p>";}
					
			$html.="</div>	<div class='span1-tablette-3-31_3'>
						<a href='modificationDocs.html' class='span1-2-48 span1-tablette-2-48'><img src='/".$http->racine."/web/images/icones/iconeModifier.png' alt='icone modifier'/></a>
						<a href='#' class='span1-2-48 span1-tablette-2-48'><img src='/".$http->racine."/web/images/icones/iconeSupprimer.png' alt='icone supprimer'/></a>							
						<form method='GET' action='#' class='span1-1-98'>
							<input id='reserver' class='bouton-rechercher' type='submit' value='Réserver'>							
						</form>	
					</div>	
					<div class='clear'></div>
					<p>Description : ".$l->description." </p>
				</div>
				<div class='clear'></div>
			</section>";
		}
	$html.="</div>
		<div class='clear'></div>
	</div>";	

	return $html;
	}	

public function afficheDetail(){
		$http = new \utils\HttpRequest();
		$html = "<div id='page-fiche-detaillee-adherents'>
			<form method='POST' action='/".$http->racine."/index.php/StaffController/listDocuments' class='span1-1-98 span1-ordi-4-23'>
			<input class='span1-1-98' name='recherche' type='text' placeholder='Mots-Clefs...' />
			
			<label class='span1-1-98'>Type : </label>
			<div class='span1-1-98'><input type='radio' name='type' value='1'>Livre</div>
			<div class='span1-1-98'><input type='radio' name='type' value='2'>CD</div><!--span1-ordi-1-98-->
			<div class='span1-1-98'><input type='radio' name='type' value='3'>DVD</div>		
			<div class='clear'></div>
			<label class='span1-1-98' for='genres'>Genre : </label>
			<select class='span1-1-98' id='genres' name='genres'>
				<option>Séléctionner un genre</option>
				<option value='1'>Action</option>
				<option value='2'>Aventure</option>
				<option value='3'>Romance</option>
				<option value='4'>Classique</option>
				<option value='5'>Informatique</option>
			</select>	
			
			<input class='span1-1-98 bouton-rechercher' id='submit' type='submit' value='Rechercher'>			
		</form>";
		
		$html.="<section class='span1-1-98 span3-ordi-4-23'>
			<img class='span1-1-98 span1-tablette-3-31_3' src='/".$http->racine."/web/images/document/".$this->list->image."' alt='document'>
			<div class='span1-tablette-3-31_3'>
				<h2>".$this->list->titre."</h2>		
				<p>État : Emprunté jusqu'au 20/12/2015</p>";
				if($this->list->id_type==1){
					$html.= "<p>Type : Livre </p>";}
				elseif($this->list->id_type==2){
					$html.= "<p>Type : CD </p>";
				}
				elseif($this->list->id_type==3){
					$html.= "<p>Type : DVD </p>";}
				
				if($this->list->id_genre==1){
					$html.= "<p>Genre : Action </p>";}
				elseif($this->list->id_genre==2){
					$html.= "<p>Genre : Aventure </p>";
				}
				elseif($this->list->id_genre==3){
					$html.= "<p>Genre : Romance </p>";}
				elseif($this->list->id_genre==4){
					$html.= "<p>Genre : Classique </p>";}
				elseif($this->list->id_genre==5){
					$html.= "<p>Genre : Informatique </p>";}
				
			$html.="	
				<p>Auteur : ".$this->list->auteur."</p>
			</div>
			<div class='span1-tablette-3-31_3'>
				<a href='modificationDocs.html' class='span1-2-48 span1-tablette-2-48'><img src='/".$http->racine."/web/images/icones/iconeModifier.png' alt='icone modifier'/></a>
						<a href='#' class='span1-2-48 span1-tablette-2-48'><img src='/".$http->racine."/web/images/icones/iconeSupprimer.png' alt='icone supprimer'/></a>							
						<form method='GET' action='#' class='span1-1-98'>
					<input id='reserver' class='bouton-rechercher' type='submit' value='Réserver'>
				</form>	
				<div class='clear'></div>
			</div>
			
			<p class='span1-1-98 span3-tablette-3-31_3'>Description : ".$this->list->description." </p>			
		</section><div class='clear'></div>
	</div>
	<div class='clear'></div>";
		return $html;
	}
	
	public function afficherReservation() {
		$html = "<section>
		<h2 class='span1-1-98'>Réserver un document</h2>
		<div class='clear'></div>
		<form method='GET' action='/add'>
			<fieldset>
				<legend class='span2-3-31_3 span1-ordi-4-23'>Emprunteur</legend>
				<div class='clear'></div>
				<label for='nAdherent' class='span1-3-31_3 span1-ordi-4-23'>N° de l'adhérent : </label>
				<input id='nAdherent' name='numadherent' type='text' class='span2-3-31_3 span1-ordi-4-23' autofocus required>
			</fieldset>
			<fieldset>
				<legend class='span2-3-31_3 span1-ordi-4-23'>Documents</legend>	
				<div class='clear'></div>
				<label for='reference' class='span1-3-31_3 span1-ordi-4-23'>Référence : </label>
				<input id='reference' name='reference[]' type='text' class='span2-3-31_3 span1-ordi-4-23' required>
				<p class='span1-1-98'>Date de début de la réservation : PHP</p>
				<div class='clear'></div>
				<p class='span1-1-98'>Date limite de réservation : PHP</p>
				
				<label for='reference' class='span1-3-31_3 span1-ordi-4-23'>Référence : </label>
				<input id='reference' name='reference[]' type='text' class='span2-3-31_3 span1-ordi-4-23' required>
				<p class='span1-1-98'>Date de début de la réservation : PHP</p>
				<div class='clear'></div>
				<p class='span1-1-98'>Date limite de réservation : PHP</p>
				
				<a href='#' class='span1-1-98'>Ajouter un document</a>
			</fieldset>
			<input id='submit' type='submit' value='Valider' class='span1-3-31_3 span1-ordi-4-23 offset1-ordi-4-23 bouton-valider'>
		</form>
		<div class='clear'></div>
	</section>
		";
		return $html;
	}
	
	public function alert($message)
	{
		echo "
            <script>
                alert(\" " . $message . "\");
            </script>
        ";
	}
	
	public function afficherIdentification() {
		$html ="<section id='page-identification-adherents'>
		<h2 class='span1-1-98'>Identification</h2>	
		<form method='GET' action='#'>
			<fieldset>
				<label class='span1-3-31_3 span1-ordi-4-23'>N° adhérent :</label>
				<input class='span2-3-31_3 span1-ordi-4-23' name='saisie' type='text' required autofocus />		
				<div class='clear'></div>
				<label class='span1-3-31_3 span1-ordi-4-23'>Mot de passe :</label>	
				<input class='span2-3-31_3 span1-ordi-4-23' name='saisie' type='text'/>					
			</fieldset>	
			<input class='offset1-3-31_3 span1-3-31_3 span1-ordi-4-23 offset1-ordi-4-23 bouton-valider' type='submit' value='Valider'/>
			<div class='clear'></div>
		</form>	
		<div class='clear'></div>
	</section>";
		return $html;
	}
	
	public function visualiserEmprunt() {
		$html = "<section id='page-visualiser-emprunts-adherents'>
		<h2 class='span1-1-98'>Vos emprunts actuels</h2>
		<p class='span1-1-98'>Veillez vous connecter pour visualiser vos emprunts</p>
		<div class='clear'></div>
	</section>";
		return $html;
	}
	
	public function afficherAccueil() {
		$http = new \utils\HttpRequest();	
		$html="	<section id='page-index-adherents'>
		<h2 class='span1-1-98'>Médiathèque MediaNet</h2>
		<img class='span1-1-98 span2-tablette-3-31_3' src='/".$http->racine."/web/images/librairie.jpg' alt='mediathèque'/>
		<p class='span1-1-98 span1-tablette-3-31_3'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu volutpat mauris. Phasellus ex diam, bibendum non lacus vitae, malesuada semper dolor. Nunc elementum gravida velit eu dictum.</p>
		
		<article class='span1-1-98'>
			<h2>Sélections à la une</h2>
			<div class='span1-1-98'>";
			
			/*foreach ($this->list as $d){	
				$html.="<a href='/".$http->racine."/indexAdherents.php/AdherentsController/ficheDetaillee'><img class='span1-1-98 span1-tablette-5-18' src='/".$http->racine."/web/images/".$d->image."' alt='livres'></a>
				<div class='span4-tablette-5-18'>
					<a href='/".$http->racine."/indexAdherents.php/AdherentsController/ficheDetaillee'><h3>".$d->Titre."</h3></a>
					<p >".$d->date_sortie." - ".$d->id_Genre."</p>
					<p>".$d->id_Type." - ".$d->Auteur."</p>
					<p>".$d->Description."</p>					
				</div>				
				</div></a>";
			}*/

		$html.="</article>
		
		<div class='span1-1-98 span2-tablette-5-18'>
			<h3>Adresse</h3>
			<p>5, rue Jean Jaurès</p>
			<p>54 000 NANCY</p>
			<h3>Contact</h3>
			<p>Tél : 03 83 37 63 29</p>
			<p>Mail : contact@medianet.fr</p>
		</div>
		<div class='span1-1-98 offset1-tablette-5-18 span2-tablette-5-18'>
			<h3>Horaires d'ouverture</h3>
			<p>Mardi et jeudi : 13h à 19h</p>
			<p>Mecredi et samedi : 10h à 18h</p>
			<p>Vendredi et dimanche : 14h à 18h</p>
			<p>Du 15 juin au 15 septembre, fermeture les dimanches et horaires réduits les mardis et jeudis (14h à 18h)</p>
		</div>
		<div class='clear'></div>
		</section>";
		
		return $html;
	}

}