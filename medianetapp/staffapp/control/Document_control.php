<?php
	namespace medianetapp\staffapp\control;
	use medianetapp\staffapp\model\Document as Document;
	require_once "../../../conf/autoload.php";
	
	$document = new Document();
	
	$document->ref_document=$_POST['ref_document'];
	$document->titre=$_POST['titre'];
	$document->description=$_POST['description'];
	$document->auteur=$_POST['auteur'];
	$document->date_sortie=$_POST['date_sortie'];
	$document->etat=$_POST['etat'];
	
	$document->save();
	
?>