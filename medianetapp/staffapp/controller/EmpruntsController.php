<?php
/**
 * Created by PhpStorm.
 * User: nayeli
 * Date: 5/11/15
 * Time: 05:09 PM
 */

namespace medianetapp\staffapp\controller;


use medianetapp\adherentsapp\model\Adherent as Adherent;
use medianetapp\staffapp\model\Document as Document;
use medianetapp\staffapp\model\Emprunt as Emprunt;
use medianetapp\staffapp\view\ViewEnregistrerEmprunt as View;
use medianetapp\staffapp\view\ViewRetournerEmprunt as ViewRetour;

class EmpruntsController {
    public $action;
    public $param;

    /*function __contruct($http) {
        $this->action = $http;
        # $this->action = $http->action;
    }*/

    function dispatch() {
        switch($this->action) {
            case 'insert' :
                $this->insertEmprunt();
                break;
            case 'retour' :
                $this->enregistrerRetour();
                break;
            default :
                echo "DEFAULT";
                break;
        }
    }

    function insertEmprunt() {
        $html = new View();

        $html->afficheHTML();
        $html->afficheHead();
        $html->afficherBody();
        $html->afficheFooter();

        if(isset($_POST['num_adh'])) {
            // Trouver l'adhérent
            $adherent = Adherent::findByNum($_POST['num_adh']);

            // Si l'adhérent existe
            if($adherent){
                // Si la duxieme reference est requise
                if(isset($_POST['ref2']) && $_POST['ref2'] != "") {
                    $document1 = Document::findByRef($_POST['ref1']);
                    $document2 = Document::findByRef($_POST['ref2']);

                    // Verifier l'existence des documents
                    // Document 1
                    if($document1) {
                        // Document 2
                        if($document2) {
                            // Trouver l'etat du document
                            if($document1->etat == 1) {
                                if($document2->etat == 1) {
                                    //ENREGISTRER EMPRUNT 1
                                    $emprunt = new Emprunt();

                                    $emprunt->id_adherent = $adherent->id_adherent;
                                    $emprunt->id_document = $document1->id;
                                    $emprunt->save();

                                    //ENREGISTRER EMPRUNT 2
                                    $emprunt->id_emprunt = null;
                                    $emprunt->id_document = $document2->id;
                                    $emprunt->save();

                                    $html->alert("L'emprunt a été enregistré");
                                } else {
                                    $html->alert("Le document " . $_POST['ref2'] . " n'est pas disponible");
                                }
                            } else {
                                $html->alert("Le document " . $_POST['ref1'] . " n'est pas disponible");
                            }

                        } else {
                            $html->alert("La référence " . $_POST['ref2'] . " n'existe pas");
                        }
                    } else {
                        $html->alert("La référence " . $_POST['ref1'] . " n'existe pas");
                    }
                } else {

                    $document = Document::findByRef($_POST['ref1']);
                    var_dump($document);
                    if($document){

                        if($document->etat == 1) {
                            $emprunt = new Emprunt();

                            $emprunt->id_adherent = $adherent->id_adherent;
                            $emprunt->id_document = $document->id;
                            $emprunt->save();

                            $html->alert("L'emprunt a été enregistré");
                        } else {
                            $html->alert("Le document " . $_POST['ref1'] . " n'est pas disponible");
                        }
                    } else {
                        $html->alert("La référence " . $_POST['ref1'] . " n'existe pas");
                    }
                }
            } else {
                $html->alert("L'adhérent n'existe pas");
            }
        }
    }

    function enregistrerRetour() {
        $html = new ViewRetour();

        $html->afficheHTML();
        $html->afficheHead();
        $html->afficherBody();
        $html->afficheFooter();

        // Pour enregistrer deux retours
        if(isset($_POST['reference2']) && $_POST['reference2'] != "") {
            //
            $document1 = Document::findByRef($_POST['reference1']);
            $document2 = Document::findByRef($_POST['reference2']);

            // Verifier l'existence des documents
            // Document 1
            if($document1) {
                // Document 2
                if($document2) {
                    // Trouver l'etat du document
                    if($document1->etat != 1) {
                        if($document2->etat != 1) {

                            //ENREGISTRER RETOUR 1
                            echo $document1->id;
                            $document1->etat = 1;
                            $document1->save();

                            //ENREGISTRER RETOUR 2

                            $html->alert("Le retour a été enregistré");
                        } else {
                            $html->alert("Erreur : Le document " . $_POST['reference2'] . " est déjà disponible");
                        }
                    } else {
                        $html->alert("Le document " . $_POST['reference1'] . " est déjà disponible");
                    }

                } else {
                    $html->alert("La référence " . $_POST['reference2'] . " n'existe pas");
                }
            } else {
                $html->alert("La référence " . $_POST['reference1'] . " n'existe pas");
            }
            //
        } else {
            echo "AQUI";
        }
    }


}


/*
libre
emprunte
indisponible*/
