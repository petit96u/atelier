<?php
namespace medianetapp\staffapp\controller;


use medianetapp\adherentsapp\model\Adherent as Adherent;
use medianetapp\staffapp\model\Document as Documents;
use medianetapp\staffapp\model\Emprunt as Emprunt;
use medianetapp\staffapp\view\ViewStaff as View;

class StaffController{
	private $action=null, $param=null;
		
	public function __construct(\utils\HttpRequest $ht_request)
		{
		$this->action= $ht_request->action;
		$this->param= $ht_request->param;
	}
		
	public function dispatch(){
		switch($this->action){
			case 'listDocuments':
			if(isset($_POST['type'])) {
				$this->searchAvancee();
			}elseif(isset($_POST['genres'])) {
				$this->searchAvancee();
			}elseif(isset($_POST['recherche'])) {
				$this->searchAvancee();
			}
			elseif(isset($_POST['mot-cle']) && $_POST['mot-cle']!=""){
				$this->search();
			}else
				
			$this->listDocuments();
			break;
			case 'createDocument':
			$this->createDocument();
			break;
			case 'editDocument':
			$this->editDocument();
			break;
			case 'deleteDocument':
			$this->deleteDocument();
			break;
			case 'gestionDocuments':
			$this->gestionDocuments();
			break;
			case 'gestionAdherents':
			$this->gestionAdherents();
			break;
			case 'listType':
			$this->listType();
			break;
			case 'listGenre':
			$this->listGenre();
			break;
			case 'reservation':
			$this->reservation();
			break;
			case 'insertEmprunt' :
            $this->insertEmprunt();
            break;
			case 'retourEmprunt' :
            $this->retourEmprunt();
            break;
			case 'findById':
			$this->findById();
            break;
			default : $this->Accueil();
				break;
		}
	}
	
	private function Accueil() {
		$view = new View();
		$view->afficher("accueil");
	}	
	
	private function listDocuments(){
		$list = Documents::findAll();
		
		if($list) {
			$view = new View();
			$view->list=$list;
			$view->afficher("listDocuments");
		}
	}
		
	private function createDocument(){
		$document = new Documents();
		$document->save();
		$view = new View();
		$view->document = $document;
		$view->afficher("createDocument");
	}
	
	private function editDocument() {
	
	}
	
	private function deleteDocument() {
		$document = new Documents();
		$document->delete();
		$view = new View();
		$view->afficher("deleteDocument");
	}
	
	private function gestionDocuments() {
		$view = new View();
		$view->afficher("gestionDocuments");
	}
	
	private function gestionAdherents() {
		$list = Adherent::findAll();
		$view = new View();
		$view->list=$list;
		$view->afficher("gestionAdherents");
	}
		
	private function listType() {
		$view = new View();
		$view->afficher("listType");
	}	
	
	private function listGenre() {
		$view = new View();
		$view->afficher("listGenre");
	}	
	
	private function search(){
		if(isset($_POST["mot-cle"]) && $_POST["mot-cle"]!=""){
		
			$document= Documents::findByTitle($_POST["mot-cle"]);
			
			if($document){
				$view = new View();
				$view->list=$document;
				$view->afficher("listDocuments");
			}else
			echo "Aucun r�sultat";
		}
	}
	
	private function searchAvancee(){
		
			// si les trois champs sont remplis, on les r�cup�re. Cela permettra d'affiner la recherche au maximum.
			/*if(isset($_POST["type"])&&(isset($_POST["genres"]))&&(isset($_POST["recherche"]))){
				$type = $_POST["type"];
				$genre = $_POST["genres"];
				$motcle = $_POST["recherche"];
				$list= Documents::findbyAll($type, $genre,$motcle);
					
				if($list){
					$view = new View();
					$view->list=$list;
					$view->afficher("listDocuments");
				}
				else{
					$view = new View();
					$view->alert("Aucun r�sultat all");
				}
			}*/
			if (isset($_POST["type"])) {
			$list= Documents::findbyType($_POST["type"]);
					if($list){
						$view = new View();
						$view->list=$list;
						$view->afficher("listDocuments");
					}
			}
			elseif(isset($_POST["genres"])){
				$list = Documents::findbyGenre($_POST["genres"]);
				if($list){
					$view = new View();
					$view->list=$list;
					$view->afficher("listDocuments");
				}	
				}
				if(isset($_POST["recherche"]) && $_POST["recherche"]!=""){
						
					$list = Documents::findbyTitle($_POST["recherche"]);
					if($list){
						$view = new View();
						$view->list=$list;
						$view->afficher("listDocuments");
					}
					else{
					
						$view = new View();
						$view->afficher("listDocuments");
						$view->alert("Aucun r�sultat recherche");
					}
				}
						
					
		}
	
	private function reservation() {
		$view = new View();
		$view->afficher("reservation");
	}
	
	private function findById() {
		$detail= Documents::findById($_GET['id']);
		if($detail){
		$view = new View();
		$view->list=$detail;
		$view->afficher("detail");}
	}
	
	function insertEmprunt() {
		$view = new View();
		$view->afficher("insertEmprunt");
        if(isset($_POST['numadherent'])) {
            // Trouver l'adh�rent
            $adherent = Adherent::findByNum($_POST['numadherent']);

            // Si l'adh�rent existe
            if($adherent){
                // Si la duxieme reference est requise
                if(isset($_POST['ref2']) && $_POST['ref2'] != "") {
                    $document1 = Documents::findByRef($_POST['ref1']);
                    $document2 = Documents::findByRef($_POST['ref2']);

                    // Verifier l'existence des documents
                    // Document 1
                    if($document1) {
                        // Document 2
                        if($document2) {
                            // Trouver l'etat du document
                            if($document1->etat == 1) {
                                if($document2->etat == 1) {
                                    //ENREGISTRER EMPRUNT 1
                                    $emprunt = new Emprunt();

                                    $emprunt->id_adherent = $adherent->id_adherent;
                                    $emprunt->id_document = $document1->id;
                                    $emprunt->save();

                                    //ENREGISTRER EMPRUNT 2
                                    $emprunt->id_emprunt = null;
                                    $emprunt->id_document = $document2->id;
                                    $emprunt->save();

                                    $view->alert("L'emprunt a �t� enregistr�");
                                } else {
                                    $view->alert("Le document " . $_POST['ref2'] . " n'est pas disponible");
                                }
                            } else {
                                $view->alert("Le document " . $_POST['ref1'] . " n'est pas disponible");
                            }

                        } else {
                            $view->alert("La r�f�rence " . $_POST['ref2'] . " n'existe pas");
                        }
                    } else {
                        $view->alert("La r�f�rence " . $_POST['ref1'] . " n'existe pas");
                    }
                } else {

                    $document = Documents::findByRef($_POST['ref1']);
                    
                    if($document){

                        if($document->etat == 1) {
                            $emprunt = new Emprunt();

                            $emprunt->id_adherent = $adherent->id_adherent;
                            $emprunt->id_document = $document->id;
                            $emprunt->save();

                            $view->alert("L'emprunt a �t� enregistr�");
                        } else {
                            $view->alert("Le document " . $_POST['ref1'] . " n'est pas disponible");
                        }
                    } else {
                        $view->alert("La r�f�rence " . $_POST['ref1'] . " n'existe pas");
                    }
                }
            } else {
                $view->alert("L'adh�rent n'existe pas");
            }
        }
		
    }
	
	function retourEmprunt() {
        $view = new View();
		$view->afficher("retourEmprunt");

        // Pour enregistrer deux retours
        if(isset($_POST['ref2']) && $_POST['ref2'] != "") {
            //
            $document1 = Documents::findByRef($_POST['ref1']);
            $document2 = Documents::findByRef($_POST['ref2']);

            // Verifier l'existence des documents
            // Document 1
            if($document1) {
                // Document 2
                if($document2) {
                    // Trouver l'etat du document
                    if($document1->etat != 1) {
                        if($document2->etat != 1) {

                            //ENREGISTRER RETOUR 1
                            $document1->etat = 1;
                            $document1->save();

                            //ENREGISTRER RETOUR 2
                            $document2->etat = 1;
                            $document2->save();

                            $view->alert("Le retour a �t� enregistr�");
                        } else {
                            $view->alert("Erreur : Le document " . $_POST['ref2'] . " est d�j� disponible");
                        }
                    } else {
                        $view->alert("Le document " . $_POST['ref1'] . " est d�j� disponible");
                    }

                } else {
                    $view->alert("La r�f�rence " . $_POST['ref2'] . " n'existe pas");
                }
            } else {
                $view->alert("La r�f�rence " . $_POST['ref1'] . " n'existe pas");
            }
            //
        } else {

            if(isset($_POST['ref1'])) {
                $document = Documents::findByRef($_POST['ref1']);
               
                if($document) {
                    if($document->etat != 1) {
                        //ENREGISTRER RETOUR
                        $document->etat = 1;
                        $document->save();

                        $view->alert("Le retour a �t� enregistr�");
                    } else {
                        $view->alert("Le document " . $_POST['ref1'] . " est d�j� disponible");
                    }
                } else {
                    $view->alert("La r�f�rence " . $_POST['ref2'] . " n'existe pas");
                }

            }
        }
    }
}