<?php

namespace medianetapp\staffapp\model;
use utils\Connexion as Connexion;

class Document {
    private $id,$ref_document,$titre,$description,$auteur,$date_sortie,$etat,$raison,$image,$archive,$id_type,$id_genre;
	
    public function __construct() {
		
    }

    public function __get($attr_name) {
		if (property_exists( __CLASS__, $attr_name)) { 
			return $this->$attr_name;
		} else
		$emess = __CLASS__ . ": unknown member $attr_name (__get)";
		throw new Exception($emess);
	}
	
	public function __set($attr_name, $attr_val) {
		if (property_exists( __CLASS__, $attr_name)) 
			$this->$attr_name=$attr_val; 
		else{
			$emess = __CLASS__ . ": unknown member $attr_name (__set)";
			throw new Exception($emess);
		}
	}
	
	public function save() {
		if (isset($this->id)) {
			return $this->update();
		} else {
  	       return $this->insert();
        }
    }	
	
  
	
	private function update() {
		if (!isset($this->id))
		return false;

		$pdo = Connexion::getConnexion();

		$query = $pdo ->prepare("update Document set ref_Document=:ref_document, Titre=:titre, Description=:description, 
		Auteur=:auteur, date_sortie=:date_sortie, Etat=:etat, Raison=:raison, Image=:image where id_Document=:id");
		
		$query->bindParam(':id',$this->id, \PDO::PARAM_INT);
		$query->bindParam(':ref_document',$this->ref_document, \PDO::PARAM_INT);
		$query->bindParam(':titre',$this->titre, \PDO::PARAM_STR);
		$query->bindParam(':description',$this->description, \PDO::PARAM_STR);
		$query->bindParam(':auteur',$this->auteur, \PDO::PARAM_STR);
        $query->bindParam(':raison' , $this->raison, \PDO::PARAM_STR);
        $query->bindParam(':etat',$this->etat, \PDO::PARAM_INT);
		
		if(isset($this->id)){
			$query->bindParam(':date_sortie',$this->date_sortie, \PDO::PARAM_STR);
		}else
            $var = null;
			$query->bindParam(':date_sortie', $var,\PDO::PARAM_STR);

		/*if(isset($this->etat)){
			$query->bindParam(':etat',$this->etat, \PDO::PARAM_INT);
		}else
            $var = null;
			$query->bindParam(':etat', $var, \PDO::PARAM_INT);*/
		
		if(isset($this->image)){
			$query->bindParam(':image',$this->image, \PDO::PARAM_STR);
		}else
            $var = null;
			$query->bindParam(':image', $var, \PDO::PARAM_STR);
		
	  
		return $query->execute();
    
	
	}
	
	private function insert() {
		$pdo = Connexion::getConnexion();
		
		$query = $pdo->prepare("INSERT INTO Document (ref_Document,Titre,Description,Auteur,date_sortie,Etat,Image)
                               VALUES (:ref_document,:titre,:description,:auteur,:date_sortie,:etat,'$this->image')");
			
		$query->bindParam(':ref_document', $this->ref_document, \PDO::PARAM_STR);
		$query->bindParam(':titre', $this->titre, \PDO::PARAM_STR);
		$query->bindParam(':description', $this->description, \PDO::PARAM_STR);
		$query->bindParam(':auteur', $this->auteur, \PDO::PARAM_STR);
		$query->bindParam(':date_sortie', $this->date_sortie, \PDO::PARAM_STR);
		$query->bindParam(':etat', $this->etat, \PDO::PARAM_STR);
		
		 if($query->execute()) {
      $this->id = $pdo->LastInsertId();
      return $this->id;
   }
   return false;
   
    }

    public function delete($id) {
		$pdo = Connexion::getConnexion();
		$query = $pdo->prepare("delete from document where id_Document=:id");
		
		if(isset($id)){
			$query->bindParam(':id',$id);
		}else{
			return 0;
		}
		
		$query->execute();
		return $query->rowCount();
	}
	
	public static function findById($id){
		$pdo = Connexion::getConnexion();
		$query = $pdo->prepare('select * from Document where id_Document=:id_document');
		$query->bindParam(':id_document',$id);
		
		if (!$query->execute()) {
			return false;
		}
		
			$rows = $query->fetch(\PDO::FETCH_OBJ);
		if(!$rows){
			return false;
		}
		$doc = new Document();
			
		$doc->id=$rows->id_Document;
		$doc->ref_document=$rows->ref_Document;
		$doc->titre=$rows->Titre;
		$doc->description=$rows->Description;
		$doc->auteur=$rows->Auteur;
		$doc->date_sortie=$rows->date_sortie;
		$doc->etat=$rows->Etat;
		$doc->raison=$rows->Raison;
		$doc->id_type=$rows->id_Type;
		$doc->id_genre=$rows->id_Genre;
		$doc->image=$rows->Image;
		return $doc;
	}
	
		public static function findByAll($idtype,$idgenre,$motcle){
		$res = array();
		$pdo = Connexion::getConnexion();
		$query = $pdo->prepare("select * from document 
		where id_Type = :idtype AND id_Genre = :idGenre AND titre LIKE :mcle");
		$motcle = "%".$motcle."%";
		$query->bindParam(":idtype", $idtype);
		$query->bindParam(":idGenre",$idgenre);
		$query->bindParam(":mcle", $motcle);
		
		
		if($query->execute()){
			$document = $query->fetchAll(\PDO::FETCH_OBJ);
			foreach($document as $doc){
				$d = new Document();
				$d->id=$doc->id_Document;
				$d->ref_document=$doc->ref_Document;
				$d->titre=$doc->Titre;
				$d->description=$doc->Description;
				$d->auteur=$doc->Auteur;
				$d->date_sortie=$doc->date_sortie;
				$d->etat=$doc->Etat;
				$d->raison=$doc->Raison;
				$d->id_type=$doc->id_Type;
				$d->id_genre=$doc->id_Genre;
				$d->image=$doc->Image;
				$res[]=$d;
			}
		}
		return $res;
	}
	
	public static function findByType($idtype){
		$res = array();
		$pdo = Connexion::getConnexion();
		$query = $pdo->prepare("select * from Document where id_Type = :idtype");
		$query->bindParam(":idtype", $idtype);
		if ($query->execute()){
			$document = $query->fetchAll(\PDO::FETCH_OBJ);
				foreach($document as $doc){
			$d = new Document();
				$d->id=$doc->id_Document;
				$d->ref_document=$doc->ref_Document;
				$d->titre=$doc->Titre;
				$d->description=$doc->Description;
				$d->auteur=$doc->Auteur;
				$d->date_sortie=$doc->date_sortie;
				$d->etat=$doc->Etat;
				$d->raison=$doc->Raison;
				$d->id_type=$doc->id_Type;
				$d->id_genre=$doc->id_Genre;
				$d->image=$doc->Image;
				$res[]=$d;
				
			}
		}
		return $res;
	}
	
		public static function findbyGenre($idgenre){
		$res=array();
		$pdo = Connexion::getConnexion();
		
		$query = $pdo->prepare("select * from Document where id_Genre = :idGenre");
		$query -> bindParam(":idGenre", $idgenre );
		if ($query->execute()){
			$document = $query->fetchAll(\PDO::FETCH_OBJ);
				foreach($document as $doc){
			$d = new Document();
				$d->id=$doc->id_Document;
				$d->ref_document=$doc->ref_Document;
				$d->titre=$doc->Titre;
				$d->description=$doc->Description;
				$d->auteur=$doc->Auteur;
				$d->date_sortie=$doc->date_sortie;
				$d->etat=$doc->Etat;
				$d->raison=$doc->Raison;
				$d->id_type=$doc->id_Type;
				$d->id_genre=$doc->id_Genre;
				$d->image=$doc->Image;
				$res[]=$d;
				
			}
			
		}
		return $res;
	}
	
	public static function findByRef($ref_document){

		$pdo = Connexion::getConnexion();
		$query = $pdo->prepare('select * from Document where ref_Document=:ref_document');
		$query->bindParam(':ref_document',$ref_document);
		
		if (!$query->execute()) {
			return false;
		}
		
			$rows = $query->fetch(\PDO::FETCH_OBJ);
		if(!$rows){
			return false;
		}
		$doc = new Document();
			
		$doc->id=$rows->id_Document;
		$doc->ref_document=$rows->ref_Document;
		$doc->titre=$rows->Titre;
		$doc->description=$rows->Description;
		$doc->auteur=$rows->Auteur;
		$doc->date_sortie=$rows->date_sortie;
		$doc->etat=$rows->Etat;
		$doc->raison=$rows->Raison;
		$doc->id_type=$rows->id_Type;
		$doc->id_genre=$rows->id_Genre;
		$doc->image=$rows->Image;
		return $doc;
		
	}
	
	public static function findByTitle($titre){
		$res=array();
		$pdo = Connexion::getConnexion();
		$query = $pdo->prepare("select * from Document where Titre like :titre");
		$titre = "%".$titre."%";
		$query->bindParam(':titre',$titre);
		
		if (!$query->execute()) {
			return false;
		}
		
		$rows = $query->fetchAll(\PDO::FETCH_OBJ);
		
			if($rows){
		
			foreach($rows as $r){
				$doc = new Document();
				
				$doc->id=$r->id_Document;
				$doc->ref_document=$r->ref_Document;
				$doc->titre=$r->Titre;
				$doc->description=$r->Description;
				$doc->auteur=$r->Auteur;
				$doc->date_sortie=$r->date_sortie;
				$doc->etat=$r->Etat;
				$doc->raison=$r->Raison;
				$doc->id_type=$r->id_Type;
				$doc->id_genre=$r->id_Genre;
				$doc->image=$r->Image;
				$res[]=$doc;
			}
			return $res;
			}
	}


	
	public static function findAll(){
		$res=array();
		$pdo = Connexion::getConnexion();
		$query = $pdo->prepare("select * from Document");
		
		if (!$query->execute()) {
			return false;
		}
		
		$rows = $query->fetchAll(\PDO::FETCH_OBJ);
		
			if($rows){
		
			foreach($rows as $r){
				$doc = new Document();
				
				$doc->id=$r->id_Document;
				$doc->ref_document=$r->ref_Document;
				$doc->titre=$r->Titre;
				$doc->description=$r->Description;
				$doc->auteur=$r->Auteur;
				$doc->date_sortie=$r->date_sortie;
				$doc->etat=$r->Etat;
				$doc->raison=$r->Raison;
				$doc->id_type=$r->id_Type;
				$doc->id_genre=$r->id_Genre;
				$doc->image=$r->Image;
				$res[]=$doc;
			}
			return $res;
		}
	}
	
	/*public static function getType(){
		$res = array();
		$pdo= Connexion::getConnexion();
		$query = prepare("select * from type where id_Type = :idType");
		$query-> bindParam(":idType", $this->id_type);
		if ($query->execute()){
			$type= $query->fetch(\PDO::FETCH_OBJ);
				foreach ($type as $ty){
				$t = new Type();
				$t->id_type = $ty->id_Type;
				$t->libelle_type = $ty->label_type;
			}	$res[]=$t;
		}
		return $res;
	}
	
	public static function getGenre(){
		$res=array();
		$pdo = Connexion::getConnexion();
		$query = prepare("select * from genre where id_Genre = :idGenre");
		$query->bindParam(":idGenre", $this->id_genre);
		if ($query->execute()){
			$genre = $query->fetch(\PDO::FETCH_OBJ);
			foreach ($genre as $g){
				$genre = new Genre();
				$genre->id_genre = $g->id_Genre;
				$genre->label_genre = $g->label_genre;
				$res[]= $genre;
			}
		}
		return $res;
	}*/
	
	

}

?>
