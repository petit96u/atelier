<?php
/**
 * Created by PhpStorm.
 * User: nayeli
 * Date: 4/11/15
 * Time: 03:44 PM
 */

namespace medianetapp\staffapp\model;
use medianetapp\adherentsapp\model\Adherent as Adherent;
use medianetapp\connexion\Connexion as Connexion;

class Emprunt {
    private $id_emprunt;
    private $date_emprunt;
    private $date_retour;
    private $id_adherent;
    private $id_document;

    /**
     *  Magic pour imprimer
     *
     *  Fonction Magic retournant une chaine de caracteres imprimable
     *  pour imprimer facilement un utilisateur pour la correction derreurs
     *
     *  @return String
     */
    public function __toString() {
        return "[". __CLASS__ . "] [id : ". $this->id .
        "] [Login : " . $this->login .
        "] [Role : " . $this->role . "]";
    }
    /**
     *   Getter generique
     *
     *   fonction d'acces aux attributs d'un objet.
     *   Recoit en parametre le nom de l'attribut accede
     *   et retourne sa valeur.
     *
     *   @param String $attr_name attribute name
     *   @return mixed
     */

    public function __get($attr_name) {
        if (property_exists( __CLASS__, $attr_name)) {
            return $this->$attr_name;
        }
        $emess = __CLASS__ . ": unknown member $attr_name (__get)";
        throw new \Exception($emess);
    }
    /**
     *   Setter generique
     *
     *   fonction de modification des attributs d'un objet.
     *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
     *
     *   @param String $attr_name attribute name
     *   @param mixed $attr_val attribute value
     */

    public function __set($attr_name, $attr_val) {
        if (property_exists( __CLASS__, $attr_name))
            $this->$attr_name=$attr_val;
        else{
            $emess = __CLASS__ . ": unknown member $attr_name (__set)";
            throw new \Exception($emess);
        }
    }

    public function save() {
        if(isset($this->id_emprunt)) {
            return $this->update();
        } else{
            $r = self::findById($this->id_emprunt);

            if($r == false) {
                return $this->insert();
            } else {
                $this->id_emprunt = $r->id_emprunt;
                return $this->update();
            }
        }
    }

    private function update() {
        if(!isset($this->id_emprunt)) {
            return false;
        }

        $pdo = Connexion::getConnexion();

        $query = $pdo->prepare("UPDATE Emprunt
                                SET date_emprunt = :date_emprunt,
                                    date_retour = :date_retour,
                                    id_Adherent = :id_adherent
                                WHERE id_emprunt = :id_emprunt");


        $query->bindParam(':id_emprunt', $this->id_emprunt);
        $query->bindParam(':date_emprunt', $this->date_emprunt, \PDO::PARAM_STR);
        $query->bindParam(':date_retour', $this->date_retour, \PDO::PARAM_STR);
        $query->bindParam(':id_adherent', $this->id_adherent);

        return $query->execute();
    }

    public function delete() {
        $pdo = Connexion::getConnexion();

        $query = $pdo->prepare("DELETE FROM Emprunt WHERE id_emprunt = :id_emprunt");

        if(isset($this->id_emprunt)) {
            $query->bindParam(':id_emprunt', $this->id_emprunt);
        } else {
            return 0;
        }

        $query->execute();

        return $query->rowCount();
    }

    public function insert() {
        $pdo = Connexion::getConnexion();

        // INSERT TABLE EMPRUNT
        $query = $pdo->prepare("INSERT INTO Emprunt (date_emprunt,
                                                     date_retour,
                                                     id_Adherent)
                                VALUES (NOW(),
                                        NOW() + INTERVAL 15 DAY,
                                        :id_adherent)");

        $query->bindParam(':id_adherent', $this->id_adherent);

        if($query->execute()) {
            $this->id_emprunt = $pdo->LastInsertId();
        }

        // INSERT TABLE ENREGISTRER
        $query2 = $pdo->prepare("INSERT INTO Enregistrer
                                VALUES (:id_document, :id_emprunt)");

        $query2->bindParam(':id_emprunt', $this->id_emprunt);
        $query2->bindParam(':id_document', $this->id_document);

        if(!$query2->execute()) {
            return false;
        }

        $query3 = $pdo->prepare("UPDATE Document SET Etat = 2 WHERE id_Document = :id_document");

        $query3->bindParam(':id_document', $this->id_document);

        if($query3->execute()) {
            return $this->id_emprunt;
        }

        return false;
    }

    public static function findById($id_emprunt) {
        $pdo = Connexion::getConnexion();

        $query = $pdo->prepare("SELECT * FROM Emprunt WHERE id_emprunt = :id_emprunt");

        $query->bindParam(':id_emprunt', $id_emprunt);

        if(!$query->execute()) {
            return false;
        }

        $e = $query->fetch(\PDO::FETCH_OBJ);

        if(!$e) {
            return false;
        }

        $emprunt = new Emprunt();

        $emprunt->id_emprunt = $e->id_Adherent;
        $emprunt->date_emprunt = $e->date_emprunt;
        $emprunt->date_retour = $e->date_retour;
        $emprunt->id_adherent = $e->id_Adherent;

        return $emprunt;
    }

    public static function findByIdAdherent($id_adherent){
        $res = array();

        $pdo = Connexion::getConnexion();

        $pdo_stm = $pdo->prepare("SELECT * FROM Emprunt WHERE id_Adherent = :id_adherent");

        $pdo_stm->bindParam(':id_adherent', $id_adherent);

        if($pdo_stm->execute()) {

            $rows = $pdo_stm->fetchAll(\PDO::FETCH_OBJ);

            foreach($rows as $e) {

                $emprunt = new Emprunt();

                $emprunt->id_emprunt = $e->id_emprunt;
                $emprunt->date_emprunt = $e->date_emprunt;
                $emprunt->date_retour = $e->date_retour;
                $emprunt->id_adherent = $e->id_Adherent;

                $res[] = $emprunt;
            }
        }

        return $res;
    }

    public static function findAll() {
        $res = array();

        $pdo = Connexion::getConnexion();

        $pdo_stm = $pdo->query("SELECT * FROM Emprunt");

        if($pdo_stm) {
            $rows = $pdo_stm->fetchAll(\PDO::FETCH_OBJ);
            foreach($rows as $e) {
                $emprunt = new Emprunt();

                $emprunt->id_emprunt = $e->id_emprunt;
                $emprunt->date_emprunt = $e->date_emprunt;
                $emprunt->date_retour = $e->date_retour;
                $emprunt->id_adherent = $e->id_Adherent;

                $res[] = $emprunt;
            }
        }

        return $res;
    }

    public function adherent() {

        $pdo = Connexion::getConnexion();

        $query = $pdo->prepare("SELECT * FROM Adherent A JOIN Emprunt E ON A.id_Adherent = E.id_Adherent WHERE E.id_emprunt = :id_emprunt");

        $query->bindParam(':id_emprunt', $this->id_emprunt);

        if(!$query->execute()) {
            return false;
        }

        $a = $query->fetch(\PDO::FETCH_OBJ);

        if(!$a) {
            return false;
        }

        $adherent = new Adherent();

        $adherent->id_adherent = $a->id_Adherent;
        $adherent->num_adherent = $a->Num_adherent;
        $adherent->nom = $a->Nom;
        $adherent->prenom = $a->Prenom;
        $adherent->adresse = $a->Adresse;
        $adherent->code_postal = $a->Code_postal;
        $adherent->ville = $a->Ville;
        $adherent->telephone = $a->Telephone;
        $adherent->mail = $a->Mail;
        $adherent->mot_de_passe = $a->Mot_de_Passe;
        $adherent->date_adhesion = $a->date_adhesion;

        return $adherent;

    }

}