<?php

namespace medianetapp\staffapp\model;
use medianetapp\connexion\Connexion as Connexion;

class Type {
    private $id_type,$label_type;
	
    public function __construct() {
		
    }

    public function __get($attr_name) {
		if (property_exists( __CLASS__, $attr_name)) { 
			return $this->$attr_name;
		} else
		$emess = __CLASS__ . ": unknown member $attr_name (__get)";
		throw new Exception($emess);
	}
	
	public function __set($attr_name, $attr_val) {
		if (property_exists( __CLASS__, $attr_name)) 
			$this->$attr_name=$attr_val; 
		else{
			$emess = __CLASS__ . ": unknown member $attr_name (__set)";
			throw new Exception($emess);
		}
	}
	
	public static function findAllType(){
		$res=array();
		$pdo = Connexion::getConnexion();
		
		$query = $pdo->prepare("select * from Type");
		
		if ($query) {
			$rows = $query->fetchAll(\PDO::FETCH_OBJ);
			foreach($rows as $r){
				$type = new Type();
				
				$type->id_type=$r->id_Type;
				$type->label_type=$r->label_type;
				$res[]=$type;
			}
		}
		
		return $res;
	}
	
	/*public static
	function findByType($id_type){
		$res=array();
		$pdo = Connexion::getConnexion();
		
		$query=$pdo->prepare('select * from Document where id_Type=:id_type');
		$query->bindParam(':id_type',$id_type);
		
		if(!$query->execute()){
			return false;
		}
		
		$rows = $query->fetchAll(\PDO::FETCH_OBJ);

		
		foreach($rows as $r){
			$doc = new Documento();
			
			$doc->id=$r->id_Document;
			$doc->ref_document=$r->ref_Document;
			$doc->titre=$r->Titre;
			$doc->description=$r->Description;
			$doc->auteur=$r->Auteur;
			$doc->date_sortie=$r->date_sortie;
			$doc->etat=$r->Etat;
			$doc->raison=$r->Raison;
			$doc->id_type=$r->id_Type;
			$doc->id_genre=$r->id_Genre;
			$doc->image=$r->Image;
			$res[]=$doc;
		}
		return $res;
		
	}*/

}

?>
