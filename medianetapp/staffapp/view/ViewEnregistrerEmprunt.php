<?php
/**
 * Created by PhpStorm.
 * User: nayeli
 * Date: 6/11/15
 * Time: 08:40 AM
 */

namespace medianetapp\staffapp\view;


class ViewEnregistrerEmprunt
{

	public function afficheHTML()
	{
		echo '
            <!DOCTYPE html>
            <html lang="fr" xmlns="http://www.w3.org/1999/xhtml">
        ';
	}

	public function afficheHead()
	{
		echo '
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width ,initial-scale=1.0">
	            <link rel="stylesheet" href="sass/stylesheets/alebrije_librairie.css" />
                <title>Medianet | Enregistrer un emprunt</title>
            </head>
        ';
	}

	public function afficherBody()
	{
		echo "
            <body>
	<header>
		<img src='web/images/' alt='logo'/>
		<h1>Plateforme pour les gestionnaires de la médiathèque</h1>
		<form id='recherche' method='post' action='recherche_annonce.html'>	<!--barre de recherche de l'entete-->
				<input name='saisie' type='text' placeholder='Mots-Clefs...' required /><!--
				--><input class='loupe' type='submit' value='' />
		</form>
		<nav>
			<ul>
				<li><a href=''>Liste documents</a></li>
				<li><a href=''>Enregistrer emprunt</a></li>
				<li><a href=''>Enregistrer retour</a></li>
				<li><a href=''>Gestion documents</a></li>
				<li><a href=''>Gestion adhérents</a></li>
				<li><a href=''>Réservation</a></li>
			</ul>
		</nav>
	</header>
	
	<section>
		<h2>Enregistrer un emprunt</h2>	
		<form method='POST' action='#'>
			<fieldset>
				<legend>Emprunteur</legend>
				<label>N° de l'adhérent :</label>
				<input name='num_adh' type='text' required autofocus/>
			</fieldset>	
			<fieldset>				
				<legend>Documents</legend>
				<label>Référence :</label>	
				<input name='ref1' type='text' required />
				<label>Référence :</label>	
				<input name='ref2' type='text'/>
				<a href='ajouterDocs.html'>Ajouter un document</a>
				<input type='submit' value='Valider' />
			</fieldset>	
		</form>		
	</section>
        ";
	}

	public function afficheFooter()
	{
		echo '
                    <footer class="row">
                        <p>Medianet.fr (c) 2015</p>
                    </footer>
                </body>
            </html>
        ';
	}

	public function alert($message)
	{
		echo "
            <script>
                alert(\" " . $message . "\");
            </script>
        ";
	}
}


	// alert(\"L'adhérent n'existe pas\");