<?php
/**
 * Created by PhpStorm.
 * User: nayeli
 * Date: 6/11/15
 * Time: 08:40 AM
 */

namespace medianetapp\staffapp\view;


class ViewRetournerEmprunt
{

	public function afficheHTML()
	{
		echo '
            <!DOCTYPE html>
			<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">
        ';
	}

	public function afficheHead()
	{
		echo '
            <head>
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width ,initial-scale=1.0">
				<link rel="stylesheet" href="web/sass/stylesheets/alebrije_librairie.css" />
				<link rel="icon" type="image/png" href="web/images/favicon.png"/>
				<script src="web/js/jquery.js" type="text/javascript"></script>
				<script src="web/js/menu.js" type="text/javascript"></script>
				<title>MediaNet | Enregistrer un retour</title>
			</head>
        ';
	}

	public function afficherBody()
	{
		echo '
			<body>
	<header>
		<div class="span2-ordi-4-23">
			<a href="indexStaff.html" class="span2-ordi-4-23"><img src="web/images/logoMedianet.png" alt="logo"/></a>
			<h1 class="span2-ordi-4-23">Plateforme pour les gestionnaires de la médiathèque</h1>
		</div>
		<div class="span2-ordi-4-23">
			<form class="span1-1-98" id="recherche" method="post" action="recherche_annonce.html">	<!--barre de recherche de lentete-->
					<input name="saisie" type="text" placeholder="Mots-Clefs..." required /><!--
					--><input class="loupe" type="submit" value="" />
			</form>
			<div class="clear"></div>
			<nav>
				<ul>
				   <li class="active span1-ordi-3-31_3"><a href="listeDocs.html"><span>Liste documents</span></a></li>
				   <li class="span1-ordi-3-31_3"><a href="enregistrerEmprunt.html"><span>Enregistrer emprunt</span></a></li>
				   <li class="span1-ordi-3-31_3"><a href="enregistrerRetour.html"><span>Enregistrer retour</span></a></li>
				   <div class="clear"></div>
				   <li class="span1-ordi-3-31_3"><a href="gestionDocs.html"><span>Gestion document</span></a></li>
				   <li class="span1-ordi-3-31_3"><a href="gestionAdherents.html"><span>Gestion adhérents</span></a></li>
				   <li class="last span1-ordi-3-31_3"><a href="reservation.html"><span>Réservation</span></a></li><div class="clear"></div>
				</ul>
			</nav>
		</div>
		<div class="clear"></div>
	</header>
	
	<section>
		<h2 class="span1-1-98">Enregistrer un retour</h2>	
		<div class="clear"></div>
		<form method="POST" action="#">
			<fieldset>
				<legend class="span2-3-31_3 span1-ordi-4-23">Documents</legend>
				<div class="clear"></div>
				<label class="span1-3-31_3 span1-ordi-4-23">Référence :</label>
				<input class="span2-3-31_3 span1-ordi-4-23" name="reference1" type="text" required autofocus/>
				<div class="clear"></div>
				<label class="span1-3-31_3 span1-ordi-4-23">Référence :</label>	
				<input class="span2-3-31_3 span1-ordi-4-23" name="reference2" type="text"/>
				<div class="clear"></div>
				<a class="span2-3-31_3 span1-ordi-4-23" href="ajouterDocs.html">Ajouter un document</a>				
			</fieldset>	
			<input class="offset1-3-31_3 span1-3-31_3 span1-ordi-4-23 offset1-ordi-4-23 bouton-valider" type="submit" value="Valider"/>
			<div class="clear"></div>
		</form>	
		<div class="clear"></div>	
	</section>
	<div class="clear"></div>
		';
	}

	public function afficheFooter()
	{
		echo '
                    <footer class="row">
                        <p>Medianet.fr (c) 2015</p>
                    </footer>
                </body>
            </html>
        ';
	}

	public function alert($message)
	{
		echo "
            <script>
                alert(\" " . $message . "\");
            </script>
        ";
	}
}


	// alert(\"L'adhérent n'existe pas\");