<?php

namespace medianetapp\staffapp\view;

class ViewStaff {
	private $document, $list=array();

public function __construct() {
  }

/*get et set pour accéder aux attr privés ds StaffControlleur*/
	public function __get($attr_name) {
	    if (property_exists( __CLASS__, $attr_name)) { 
	      return $this->$attr_name;
	    } 
	    $emess = __CLASS__ . ": unknown member $attr_name (__get)";
	    throw new \Exception($emess);
	}

	public function __set($attr_name, $attr_val) {
	    if (property_exists( __CLASS__, $attr_name)) 
	      $this->$attr_name=$attr_val; 
	    else{
	      $emess = __CLASS__ . ": unknown member $attr_name (__set)";
	      throw new \Exception($emess);
	    }
	}
	public function afficher($nomAction) {
		
		$header=$this->afficherHeader();
		$footer=$this->afficherFooter();
		
		switch ($nomAction){
			case 'listDocuments' :
			$main=$this->listDocuments();
			break;
			case 'createDocument' :
			$main=$this->afficherCreateDocument();	
			break;
			case 'editDocuments' :
			$main=$this->afficherEditDocument();
			break;
			case 'deleteDocument' :
			$main=$this->afficherDeleteDocument();
			break;
			case 'gestionDocuments':
			$main=$this->afficherGestionDocuments();
			break;
			case 'gestionAdherents':
			$main=$this->afficherGestionAdherents();
			break;
			case 'listType':
			$main=$this->afficherListType();
			break;
			case 'listGenre':
			$main=$this->afficherListGenre();
			break;
			case 'reservation';
			$main=$this->afficherReservation();
			break;
			case 'detail';
			$main=$this->afficheDetail();
			break;
			case 'insertEmprunt' :
			$main=$this->afficherInsertEmprunt();
			break;			
			case 'retourEmprunt' :
			$main=$this->afficherRetourEmprunt();
			break;
			case 'accueil' :
			$main=$this->afficherAccueil();
			break;
		}	

		$http = new \utils\HttpRequest();

		$html="
		<!DOCTYPE html>
		<html lang='fr' xmlns='http://www.w3.org/1999/xhtml'>
		<head>
			<meta charset='utf-8'>
			<meta name='viewport' content='width=device-width ,initial-scale=1.0'>
			<link rel='stylesheet' href='/".$http->racine."/web/sass/stylesheets/alebrije_librairie.css' />
			<link rel='icon' href='/".$http->racine."/web/images/favicon.png' type='image/png' />
			<script src='/".$http->racine."/web/js/jquery.js' type='text/javascript'></script>
			<script src='/".$http->racine."/web/js/menu.js' type='text/javascript'></script>
			<title>MediaNet</title>
		</head>
		<body>
			<header>".$header."</header>"
			.$main.
			"<footer>".$footer."</footer>
		</body>
		</html>";
		echo $html;
	}

	public function afficherHeader() {
		$http = new \utils\HttpRequest();
		$html = "
		<div class='span2-ordi-4-23'>
			<a href='/".$http->racine."/index.php' class='span2-ordi-4-23'><img src='/".$http->racine."/web/images/logoMediaNet.png' alt='logo'/></a>
			<h1 class='span2-ordi-4-23'>Plateforme pour les gestionnaires de la médiathèque</h1>
		</div>
		<div class='span2-ordi-4-23'>
			<form class='span1-1-98' id='recherche' method='POST' action=''>	<!--barre de recherche de l'entete-->
					<input name='mot-cle' type='text' placeholder='Mots-Clefs...' required /><!--
					--><input class='loupe' type='submit' value='' />
			</form>
			<div class='clear'></div>
			<nav>
				<ul>
				   <li class='active span1-ordi-3-31_3'><a href='/".$http->racine."/index.php/StaffController/listDocuments'><span>Liste documents</span></a></li>
				   <li class='span1-ordi-3-31_3'><a href='/".$http->racine."/index.php/StaffController/insertEmprunt'><span>Enregistrer emprunt</span></a></li>
				   <li class='span1-ordi-3-31_3'><a href='/".$http->racine."/index.php/StaffController/retourEmprunt'><span>Enregistrer retour</span></a></li>
				   <div class='clear'></div>
				   <li class='span1-ordi-3-31_3'><a href='/".$http->racine."/index.php/StaffController/gestionDocuments'><span>Gestion document</span></a></li>
				   <li class='span1-ordi-3-31_3'><a href='/".$http->racine."/index.php/StaffController/gestionAdherents'><span>Gestion adhérents</span></a></li>
				   <li class='last span1-ordi-3-31_3'><a href='/".$http->racine."/index.php/StaffController/reservation'><span>Réservation</span></a></li>
				   <div class='clear'></div>
				</ul>
			</nav>
		</div>
		<div class='clear'></div>";
		return $html;
	}

	public function afficherFooter() {
		return "<p>MediaNet.fr (c) 2015</p>";
	}

	public function listDocuments() {
		$http = new \utils\HttpRequest();
		$html = "<div class='page-liste-documents'>
			<form method='POST' action='/".$http->racine."/index.php/StaffController/listDocuments' class='span1-1-98 span1-ordi-4-23'>
			<input class='span1-1-98' name='recherche' type='text' placeholder='Mots-Clefs...' />
			
			<label class='span1-1-98'>Type : </label>
			<div class='span1-1-98'><input type='radio' name='type' value='1'>Livre</div>
			<div class='span1-1-98'><input type='radio' name='type' value='2'>CD</div><!--span1-ordi-1-98-->
			<div class='span1-1-98'><input type='radio' name='type' value='3'>DVD</div>		
			<div class='clear'></div>
			<label class='span1-1-98' for='genres'>Genre : </label>
			<select class='span1-1-98' id='genres' name='genres'>
				<option>Séléctionner un genre</option>
				<option value='1'>Action</option>
				<option value='2'>Aventure</option>
				<option value='3'>Romance</option>
				<option value='4'>Classique</option>
				<option value='5'>Informatique</option>
			</select>	
			
			<input class='span1-1-98 bouton-rechercher' id='submit' type='submit' value='Rechercher'>			
		</form>		

		<div class='span3-ordi-4-23'>";
		
		foreach ($this->list as $l) {
			$html.= "
			<section>
				<a href='findById?id=".$l->id."'><img class='span1-1-98 span1-tablette-3-31_3' src='/".$http->racine."/web/images/document/".$l->image."' alt='livre'></a>
				<div class='span2-tablette-3-31_3'>
					<div class='span2-tablette-3-31_3'>
						<h2>".$l->titre."</h2>";
			if($l->etat==1){
			$html.= "Disponible";}
			elseif($l->etat==2){
			$html.= "Emprunté jusqu'au ";
			
			}
			elseif($l->etat==3){
			$html.= "Indisponible";}
			$html.= "</p>";
			if($l->id_type==1){
					$html.= "<p>Type : Livre </p>";}
				elseif($l->id_type==2){
					$html.= "<p>Type : CD </p>";
				}
				elseif($l->id_type==3){
					$html.= "<p>Type : DVD </p>";}
				
				if($l->id_genre==1){
					$html.= "<p>Genre : Action </p>";}
				elseif($l->id_genre==2){
					$html.= "<p>Genre : Aventure </p>";
				}
				elseif($l->id_genre==3){
					$html.= "<p>Genre : Romance </p>";}
				elseif($l->id_genre==4){
					$html.= "<p>Genre : Classique </p>";}
				elseif($l->id_genre==5){
					$html.= "<p>Genre : Informatique </p>";}
					
			$html.="</div>	<div class='span1-tablette-3-31_3'>
						<a href='modificationDocs.html' class='span1-2-48 span1-tablette-2-48'><img src='/".$http->racine."/web/images/icones/iconeModifier.png' alt='icone modifier'/></a>
						<a href='#' class='span1-2-48 span1-tablette-2-48'><img src='/".$http->racine."/web/images/icones/iconeSupprimer.png' alt='icone supprimer'/></a>							
						<form method='GET' action='#' class='span1-1-98'>
							<input id='reserver' class='bouton-rechercher' type='submit' value='Réserver'>							
						</form>	
					</div>	
					<div class='clear'></div>
					<p>Description : ".$l->description." </p>
				</div>
				<div class='clear'></div>
			</section>";
		}
	$html.="</div>
		<div class='clear'></div>
	</div>";	

	return $html;
	}

	public function afficherCreateDocument()  {
	$http = new \utils\HttpRequest();
		$html = "<section>
		<h2 class='span1-1-98'>Ajouter un document</h2>
		
		<form method='POST' action='/".$http->racine."index.php/StaffController/saveDocument/".$this->document->titre."'>
			<fieldset class='span1-ordi-2-48'>
				<legend class='span2-3-31_3'>Informations générales :</legend>
				<div class='clear'></div>			
				<label class='span1-3-31_3' for='titre'>Titre : </label>
				<input class='span2-3-31_3' id='titre' name='titre' type='text' autofocus required>
				<div class='clear'></div>				
				<label class='span1-3-31_3'>Description : </label>
				<textarea class='span3-3-31_3 span2-tablette-3-31_3' rows='4' cols='50'></textarea>
				<label class='span1-3-31_3' for='auteur'>Auteur : </label>
				<input class='span2-3-31_3' id='auteur' name='auteur' type='text' required>
				<div class='clear'></div>
				<label class='span1-3-31_3' for='images'>Images : </label>
				<input class='span2-3-31_3' id='images' name='images' type='file'>
				<div class='clear'></div>
				<label class='span1-3-31_3' for='date-sortie'>Date sortie : </label>
				<input class='span2-3-31_3' id='date-sortie' name='date-sortie' type='date' required>
				<div class='clear'></div>				
				<label class='span1-3-31_3' for='reference'>Référence : </label>
				<input class='span2-3-31_3' id='reference' name='reference' type='text' required>
			</fieldset>
			<fieldset class='span1-ordi-2-48'>
				<legend class='span2-3-31_3'>Types / Genre du document :</legend>	
				<div class='clear'></div>			
				<label class='span1-3-31_3' for='type'>Type : </label>
				<select class='span2-3-31_3' id='type' name='type'>
					<option value='livres'>Livres</option>
					<option value='cds'>CDs</option>
					<option value='dvds'>DVDs</option>
				</select>
				
				<div class='clear'></div>						
				<label class='span1-3-31_3' for='genre'>Genre : </label>
				<select class='span2-3-31_3' id='genre' name='genre'>
					<option value='action'>Action</option>
					<option value='aventure'>Aventure</option>
					<option value='classique'>Classique</option>
					<option value='policier'>Policier</option>
					<option value='romance'>Romance</option>
				</select>
			</fieldset>
			<fieldset class='span1-ordi-2-48'>
				<legend class='span2-3-31_3'>Etat du document :</legend>	
				<div class='clear'></div>		
				<label class='span1-3-31_3' for='etat'>Etat : </label>
				<select class='span2-3-31_3' id='etat' name='etat'>
					<option value='disponible'>Disponible</option>
					<option value='emprunte'>Emprunté</option>
					<option value='indisponible'>Indisponible</option>
				</select>	
				<div class='clear'></div>	
				<label class='span1-3-31_3' for='raison-indisponibilite'>Raison indisponibilité : </label>
				<select class='span2-3-31_3' id='raison-indisponibilite' name='raison-indisponibilite'>
					<option value='perdu'>Perdu</option>
					<option value='reparation'>Réparation</option>
					<option value='reserve'>Réservé</option>
				</select>
			</fieldset>
			<input class='offset1-3-31_3 span1-3-31_3 bouton-valider' id='submit' type='submit' value='Enregistrer'>
			<div class='clear'></div>
		</form>
		<div class='clear'></div>
	</section>";
	return $html;
	}

	public function afficherEditDocument(){
		
	}

	public function afficheDetail(){
		$http = new \utils\HttpRequest();
		$html = "<div id='page-fiche-detaillee-adherents'>
			<form method='POST' action='/".$http->racine."/index.php/StaffController/listDocuments' class='span1-1-98 span1-ordi-4-23'>
			<input class='span1-1-98' name='recherche' type='text' placeholder='Mots-Clefs...' />
			
			<label class='span1-1-98'>Type : </label>
			<div class='span1-1-98'><input type='radio' name='type' value='1'>Livre</div>
			<div class='span1-1-98'><input type='radio' name='type' value='2'>CD</div><!--span1-ordi-1-98-->
			<div class='span1-1-98'><input type='radio' name='type' value='3'>DVD</div>		
			<div class='clear'></div>
			<label class='span1-1-98' for='genres'>Genre : </label>
			<select class='span1-1-98' id='genres' name='genres'>
				<option>Séléctionner un genre</option>
				<option value='1'>Action</option>
				<option value='2'>Aventure</option>
				<option value='3'>Romance</option>
				<option value='4'>Classique</option>
				<option value='5'>Informatique</option>
			</select>	
			
			<input class='span1-1-98 bouton-rechercher' id='submit' type='submit' value='Rechercher'>			
		</form>";
		
		$html.="<section class='span1-1-98 span3-ordi-4-23'>
			<img class='span1-1-98 span1-tablette-3-31_3' src='/".$http->racine."/web/images/document/".$this->list->image."' alt='document'>
			<div class='span1-tablette-3-31_3'>
				<h2>".$this->list->titre."</h2>		
				<p>État : Emprunté jusqu'au 20/12/2015</p>";
				if($this->list->id_type==1){
					$html.= "<p>Type : Livre </p>";}
				elseif($this->list->id_type==2){
					$html.= "<p>Type : CD </p>";
				}
				elseif($this->list->id_type==3){
					$html.= "<p>Type : DVD </p>";}
				
				if($this->list->id_genre==1){
					$html.= "<p>Genre : Action </p>";}
				elseif($this->list->id_genre==2){
					$html.= "<p>Genre : Aventure </p>";
				}
				elseif($this->list->id_genre==3){
					$html.= "<p>Genre : Romance </p>";}
				elseif($this->list->id_genre==4){
					$html.= "<p>Genre : Classique </p>";}
				elseif($this->list->id_genre==5){
					$html.= "<p>Genre : Informatique </p>";}
				
			$html.="	
				<p>Auteur : ".$this->list->auteur."</p>
			</div>
			<div class='span1-tablette-3-31_3'>
				<a href='modificationDocs.html' class='span1-2-48 span1-tablette-2-48'><img src='/".$http->racine."/web/images/icones/iconeModifier.png' alt='icone modifier'/></a>
						<a href='#' class='span1-2-48 span1-tablette-2-48'><img src='/".$http->racine."/web/images/icones/iconeSupprimer.png' alt='icone supprimer'/></a>							
						<form method='GET' action='#' class='span1-1-98'>
					<input id='reserver' class='bouton-rechercher' type='submit' value='Réserver'>
				</form>	
				<div class='clear'></div>
			</div>
			
			<p class='span1-1-98 span3-tablette-3-31_3'>Description : ".$this->list->description." </p>			
		</section><div class='clear'></div>
	</div>
	<div class='clear'></div>";
		return $html;
	}
	
	public function afficherDeleteDocument() {
		$html = "<script>console.log('Le document à bien été supprimé')</script>";
		return $html;
	}
	
	public function afficherListType() {
		$html = "<div id='page-listeAjout-type-staff'>
		<div class='span1-1-98 span1-ordi-2-48'>
			<h2>Liste des types</h2>
			<form id='recherche-type' method='post' action='recherche_liste.php'>
					<input name='saisie' type='text' placeholder='Mots-Clefs...' class='span2-3-31_3' required /><!--
					--><input class='span1-3-31_3 bouton-rechercher' type='submit' value='Rechercher' />
					<div class='clear'></div>
			</form>
			<ul class='span1-1-98'>
				<li>Livre</li>
				<li>CDs</li>
				<li>DVDs</li>
			</ul>
		</div>
		
		<div class='span1-1-98 span1-ordi-2-48'>
			<h2>Ajout d'un type</h2>
			<form method='GET' action='/add'>
				<label for='intitule-du-type' class='span1-3-31_3'>Intitulé du type : </label>
				<input id='intitule-du-type' name='intitule-du-type' type='text' class='span2-3-31_3' autofocus required>
				<input class='span1-3-31_3 bouton-valider' id='submit' type='submit' value='Valider'>
				<div class='clear'></div>
			</form>
		</div>
		<div class='clear'></div>
	</div>
	<div class='clear'></div>";
	return $html;
	}
	
	public function afficherListGenre() {
		$html = "<div id='page-listeAjout-genre-staff'>
		<div class='span1-1-98 span1-ordi-2-48'>
			<h2>Liste des genres</h2>
			<form id='recherche-genre' method='post' action='recherche_genre.php'>
					<input name='saisie' type='text' placeholder='Mots-Clefs...' class='span2-3-31_3' required /><!--
					--><input class='span1-3-31_3 bouton-rechercher' type='submit' value='Rechercher' />
					<div class='clear'></div>
			</form>
			
			<ul class='span1-1-98'>
				<li>Action</li>
				<li>Aventure</li>
				<li>Policier</li>
				<li>Fantastique</li>
				<li>Dessins animés</li>
				<li>Science fiction</li>
			</ul>
		</div>
		
		<div class='span1-1-98 span1-ordi-2-48'>
			<h2>Ajout d'un genre</h2>
			<form method='GET' action='/add'>
				<label for='intitule-du-genre' class='span1-3-31_3'>Intitulé du genre : </label>
				<input id='intitule-du-genre' name='intitule-du-genre' type='text' class='span2-3-31_3' autofocus required>
				<input class='span1-3-31_3 bouton-valider' id='submit' type='submit' value='Valider'>
				<div class='clear'></div>
			</form>
		</div>
		<div class='clear'></div>
	</div>
	<div class='clear'></div>";
		return $html;
	}
	
	public function afficherGestionDocuments() {
		$http = new \utils\HttpRequest();
		$html = "<div id='page-gestion-docs-staff'>
		<div class='span1-1-98 span1-ordi-3-31_3'>
			<h2>Gestion des documents</h2>
			<p><a href='/".$http->racine."/index.php/StaffController/createDocument/'>Ajouter un document</a></p>
			<p><a href='/".$http->racine."/index.php/StaffController/listDocuments/'>Modifier un document</a></p>
			<p><a href='/".$http->racine."/index.php/StaffController/listDocuments/'>Supprimer un document</a></p>
			<p><a href='/".$http->racine."/index.php/StaffController/listDocuments/'>Lister des documents</a></p>	
		</div>
		
		<div class='span1-1-98 span1-ordi-3-31_3'>
			<h2>Gestion des types</h2>
			<p><a href='/".$http->racine."/index.php/StaffController/listType/'>Ajouter un type</a></p>
			<p><a href='/".$http->racine."/index.php/StaffController/listType/'>Lister des types</a></p>	
		</div>	
		
		<div class='span1-1-98 span1-ordi-3-31_3'>
			<h2>Gestion des genres</h2>
			<p><a href='/".$http->racine."/index.php/StaffController/listGenre/'>Ajouter un genre</a></p>
			<p><a href='/".$http->racine."/index.php/StaffController/listGenre/'>Lister des genres</a></p>	
		</div>	
		<div class='clear'></div>
	</div>
	<div class='clear'></div>
		";
		return $html;
	}	
	
	public function afficherGestionAdherents() {
		$http = new \utils\HttpRequest();	
		$html = "<div id='page-gestion-adherents-staff'>
		<form id='recherche' method='post' class='span1-1-98 span2-ordi-3-31_3' action='recherche_annonce.html'>	<!--barre de recherche de l'entete-->
			<input class='span3-4-23 span2-ordi-3-31_3' name='saisie' type='text' placeholder='Rechercher un adhérent' required autofocus/><!--
			--><input class='span1-4-23 span1-ordi-3-31_3 bouton-rechercher' type='submit' value='Rechercher' />		
		</form>		
		
		<div class='span1-ordi-3-31_3'>
			<img class='span1-4-23' src='/".$http->racine."/web/images/icones/iconeAjouter.png' alt='icone ajout'/>
			<a href='/".$http->racine."/index.php/StaffController/createAdherents/' class='span1-3-23'><p>Ajouter un adhérent</p></a>
			<div class='clear'></div>
		</div>	
		<div class='clear'></div>";
		foreach ($this->list as $l) {
			$html.= "
			<div class='span1-1-98 span1-ordi-3-31_3'>
				<div>".$l->prenom." ".$l->nom."</div>
				<p>N° adhérent : ".$l->num_adherent."</p>
				<p>Date d'adhésion : ".$l->date_adhesion."</p>
				<p>Tel : ".$l->telephone."</p>	
				<p>Mail : ".$l->mail."</p>		
				<p>Adresse : ".$l->adresse."</p>	
				<p>".$l->code_postal." ".$l->ville."</p>
				<a href='/".$http->racine."/index.php/StaffController/editAdherents/' class='span1-2-48'><img src='/".$http->racine."/web/images/icones/iconeModifier.png' alt='icone modifier'/></a>
				<a href='#' class='span1-2-48'><img src='/".$http->racine."/web/images/icones/iconeSupprimer.png' alt='icone supprimer'/></a>	
			</div>";
		}
		
		$html.="<div class='clear'></div>
	</div>
	<div class='clear'></div>";
	return $html;
	}
	
	public function afficherReservation() {
		$html = "<section>
		<h2 class='span1-1-98'>Réserver un document</h2>
		<div class='clear'></div>
		<form method='GET' action='/add'>
			<fieldset>
				<legend class='span2-3-31_3 span1-ordi-4-23'>Emprunteur</legend>
				<div class='clear'></div>
				<label for='nAdherent' class='span1-3-31_3 span1-ordi-4-23'>N° de l'adhérent : </label>
				<input id='nAdherent' name='numadherent' type='text' class='span2-3-31_3 span1-ordi-4-23' autofocus required>
			</fieldset>
			<fieldset>
				<legend class='span2-3-31_3 span1-ordi-4-23'>Documents</legend>	
				<div class='clear'></div>
				<label for='reference' class='span1-3-31_3 span1-ordi-4-23'>Référence : </label>
				<input id='reference' name='reference[]' type='text' class='span2-3-31_3 span1-ordi-4-23' required>
				<p class='span1-1-98'>Date de début de la réservation : PHP</p>
				<div class='clear'></div>
				<p class='span1-1-98'>Date limite de réservation : PHP</p>
				
				<label for='reference' class='span1-3-31_3 span1-ordi-4-23'>Référence : </label>
				<input id='reference' name='reference[]' type='text' class='span2-3-31_3 span1-ordi-4-23' required>
				<p class='span1-1-98'>Date de début de la réservation : PHP</p>
				<div class='clear'></div>
				<p class='span1-1-98'>Date limite de réservation : PHP</p>
				
				<a href='#' class='span1-1-98'>Ajouter un document</a>
			</fieldset>
			<input id='submit' type='submit' value='Valider' class='span1-3-31_3 span1-ordi-4-23 offset1-ordi-4-23 bouton-valider'>
		</form>
		<div class='clear'></div>
	</section>
		";
		return $html;
	}
	
	public function afficherInsertEmprunt() {
		$http = new \utils\HttpRequest();
		$html = "	<section>
		<h2 class='span1-1-98'>Enregistrer un emprunt</h2>	
		<div class='clear'></div>
		<form method='POST' action='/".$http->racine."/index.php/StaffController/insertEmprunt/'>
			<fieldset>
				<legend class='span2-3-31_3 span1-ordi-4-23'>Emprunteur</legend>
				<div class='clear'></div>
				<label class='span1-3-31_3 span1-ordi-4-23'>N° de l'adhérent :</label>
				<input class='span2-3-31_3 span1-ordi-4-23' name='numadherent' type='text' required autofocus/>
			</fieldset>	
			
			<fieldset>				
				<legend class='span2-3-31_3 span1-ordi-4-23'>Documents</legend>
				<div class='clear'></div>
				<label class='span1-3-31_3 span1-ordi-4-23'>Référence :</label>	
				<input class='span2-3-31_3 span1-ordi-4-23' name='ref1' type='text' required />	
				<div class='clear'></div>
				<label class='span1-3-31_3 span1-ordi-4-23'>Référence :</label>	
				<input class='span2-3-31_3 span1-ordi-4-23' name='ref2' type='text'/>
				<div class='clear'></div>
				<a href='ajouterDocs.html' class='span2-3-31_3 span1-ordi-4-23'>Ajouter un document</a>			
				<div class='clear'></div>				
			</fieldset>	
			<input type='submit' class='offset1-3-31_3 span1-3-31_3 span1-ordi-4-23 offset1-ordi-4-23 bouton-valider' value='Valider' />
			<div class='clear'></div>	
		</form>		
	</section>";
	return $html;
	}
	
	public function afficherRetourEmprunt() {
		$http = new \utils\HttpRequest();
		$html = "<section>
		<h2 class='span1-1-98'>Enregistrer un retour</h2>	
		<div class='clear'></div>
		<form method='POST' action='/".$http->racine."/index.php/StaffController/retourEmprunt/'>
			<fieldset>
				<legend class='span2-3-31_3 span1-ordi-4-23'>Documents</legend>
				<div class='clear'></div>
				<label class='span1-3-31_3 span1-ordi-4-23'>Référence :</label>
				<input class='span2-3-31_3 span1-ordi-4-23' name='ref1' type='text' required autofocus/>
				<div class='clear'></div>
				<label class='span1-3-31_3 span1-ordi-4-23'>Référence :</label>	
				<input class='span2-3-31_3 span1-ordi-4-23' name='ref2' type='text'/>	
				<div class='clear'></div>
				<a class='span2-3-31_3 span1-ordi-4-23' href='ajouterDocs.html'>Ajouter un document</a>				
			</fieldset>	
			<input class='offset1-3-31_3 span1-3-31_3 span1-ordi-4-23 offset1-ordi-4-23 bouton-valider' type='submit' value='Valider'/>
			<div class='clear'></div>
		</form>	
		<div class='clear'></div>	
	</section>";
	return $html;
	}
	
		public function alert($message) {
		echo "
            <script>
                alert(\" " . $message . "\");
            </script>
        ";
	}
	
	public function afficherAccueil() {		
		$http = new \utils\HttpRequest();
		$html = "<div id='page-accueil-staff'>
		
		<div><a href='/".$http->racine."/index.php/StaffController/listDocuments' class='span1-ordi-2-48'>			
			<img src='/".$http->racine."/web/images/icones/list.png' alt='icône liste des documents'/>
			<h2>Liste des documents</h2>
			<p>Visualiser la liste des documents de la médiathèque</p>
		</a></div>
		
		<div><a href='/".$http->racine."/index.php/StaffControler/insertEmprunt' class='span1-ordi-2-48'>
			<img src='/".$http->racine."/web/images/icones/enregistrerEmprunt.png' alt='icône enregister un emprunt'/>
			<h2>Enregistrer un emprunt</h2>
			<p>Enregistrer en toute simplicité un emprunt</p>
			<div class='clear'></div>
		</a></div>
		<div class='clear'></div>
		<div><a href='/".$http->racine."/index.php/StaffControler/retourEmprunt' class='span1-ordi-2-48'>
			<img src='/".$http->racine."/web/images/icones/enregistrerRetour.png' alt='icône enregistrer un retour'/>
			<h2>Enregistrer un retour</h2>
			<p>Enregistrer facilement le retour d'un document</p>
		</a></div>
		<div><a href='/".$http->racine."/index.php/StaffControler/gestionDocuments' class='span1-ordi-2-48'>
			<img src='/".$http->racine."/web/images/icones/gestionDocs.png' alt='icône gestion des documents'/>
			<h2>Gestion des documents</h2>
			<p>Gérer les documents, types et genres</p>
		</a></div>
		<div class='clear'></div>
		<div><a href='/".$http->racine."/index.php/StaffControler/gestionAdherents' class='span1-ordi-2-48'>
			<img src='/".$http->racine."/web/images/icones/gestionAdherents.png' alt='icône gestion des adhérents'/>
			<h2>Gestion des adhérents</h2>
			<p>Ajouter, supprimer et modifier les adhérents</p>
		</a></div>
		<div><a href='/".$http->racine."/index.php/StaffControler/reservation' class='span1-ordi-2-48'>
			<img src='/".$http->racine."/web/images/icones/reservation.png' alt='icône réservation'/>
			<h2>Réservation</h2>
			<p>Réserver un document pour un adhérent</p>
		</a></div>		
		<div class='clear'></div>
	</div>";	
		
		return $html;
		
	}

}