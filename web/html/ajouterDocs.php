<!DOCTYPE html>
<html lang="fr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width ,initial-scale=1.0">
	<link rel="stylesheet" href="sass/stylesheets/alebrije_librairie.css" />
    <title>MediaNet | Ajouter un document</title>
</head>
<body>
	<header>
		<a href="indexStaff.html"><img src="web/images/logoMediaNet.png" alt="logo"/></a>
		<h1>Plateforme pour les gestionnaires de la médiathèque</h1>
		<form id="recherche" method="post" action="recherche_annonce.html">	<!--barre de recherche de l'entete-->
				<input name="saisie" type="text" placeholder="Mots-Clefs..." required /><!--
				--><input class="loupe" type="submit" value="" />
		</form>
		<nav>
			<input type="checkbox" id="toggle-nav" title="menu">
			<label for="toggle-nav" onclick></label>
			
			<ul>
				<li><a href="listeDocs.html">Liste documents</a></li>
				<li><a href="enregistrerEmprunt.html">Enregistrer emprunt</a></li>
				<li><a href="enregistrerRetour.html">Enregistrer retour</a></li>
				<li><a href="gestionDocs.html">Gestion documents</a></li>
				<li><a href="gestionAdherents.html">Gestion adhérents</a></li>
				<li><a href="reservation.html">Réservation</a></li>
			</ul>
		</nav>
	</header>
	
	<section id="page-ajout-documents-staff">
		<h2 class="row">Ajouter un document</h2>
		
		<form method="POST" action="../../Document-test.php" enctype="multipart/form-data">
			<fieldset class="span1-ordi-2-48">
				<legend class="span2-3-31_3">Informations générales :</legend>
				<div class="clear"></div>			
				<label class="span1-3-31_3" for="titre">Titre : </label>
				<input class="span2-3-31_3" id="titre" name="titre" type="text" autofocus required>
				<div class="clear"></div>				
				<label class="span1-3-31_3">Description : </label>
				<textarea class="span3-3-31_3 span2-tablette-3-31_3" name="description" rows="4" cols="50"></textarea>
				<label class="span1-3-31_3" for="auteur">Auteur : </label>
				<input class="span2-3-31_3" id="auteur" name="auteur" type="text" required>
				<div class="clear"></div>
				<label class="span1-3-31_3" for="images">Images : </label>
				<input class="span2-3-31_3" id="images" name="image" type="file">
				<div class="clear"></div>
				<label class="span1-3-31_3" for="date-sortie">Date sortie : </label>
				<input class="span2-3-31_3" id="date-sortie" name="date_sortie" type="date" required>
				<div class="clear"></div>				
				<label class="span1-3-31_3" for="reference">Référence : </label>
				<input class="span2-3-31_3" id="reference" name="ref_document" type="text" required>
			</fieldset>
			<fieldset class="span1-ordi-2-48">
				<legend class="span2-3-31_3">Types / Genre du document :</legend>	
				<div class="clear"></div>			
				<label class="span1-3-31_3" for="type">Type : </label>
				<select class="span2-3-31_3" id="type" name="type">
					<?php
						use medianetapp\staffapp\model\Type as Type;
						$type= Type::findAllType();
						foreach($type as $t){
							echo "<option value='".$t->id_type ."'>".$t->label_type ."</option>";
						}
					?>
				</select>
				</div>
				<div class="clear"></div>						
				<label class="span1-3-31_3" for="genre">Genre : </label>
				<select class="span2-3-31_3" id="genre" name="genre">
					
				</select>
			</fieldset>
			<fieldset class="span1-ordi-2-48">
				<legend class="span2-3-31_3">Etat du document :</legend>	
				<div class="clear"></div>		
				<label class="span1-3-31_3" for="etat">Etat : </label>
				<select class="span2-3-31_3" id="etat" name="etat">
					<option value="disponible">Disponible</option>
					<option value="emprunte">Emprunté</option>
					<option value="indisponible">Indisponible</option>
				</select>	
				<div class="clear"></div>	
				<label class="span1-3-31_3" for="raison-indisponibilite">Raison indisponibilité : </label>
				<select class="span2-3-31_3" id="raison-indisponibilite" name="raison-indisponibilite">
					<option value="perdu">Perdu</option>
					<option value="reparation">Réparation</option>
					<option value="reserve">Réservé</option>
				</select>
			</fieldset>
			<input class="offset1-3-31_3 span1-3-31_3" id="submit" type="submit" value="Enregistrer">
			<div class="clear"></div>
		</form>
		<div class="clear"></div>
	</section>
	<div class="clear"></div>
	
	<footer>
		<p>MediaNet.fr (c) 2015</p>
	</footer>
	
</body>
</html>